
-- On crée les différents utilisateurs

INSERT INTO Utilisateur VALUES ('cuisinier1', 'cuisinier');
INSERT INTO Utilisateur VALUES ('maitre1', 'maitre');
INSERT INTO Utilisateur VALUES ('serveur1', 'serveur');
INSERT INTO Utilisateur VALUES ('serveur2', 'serveur');
INSERT INTO Utilisateur VALUES ('serveur3', 'serveur');
INSERT INTO Utilisateur VALUES ('assistant1', 'assistant');
INSERT INTO Utilisateur VALUES ('assistant2', 'assistant');
INSERT INTO Utilisateur VALUES ('assistant3', 'assistant');
INSERT INTO Utilisateur VALUES ('directeur', 'directeur');

-- Les stocks

-- Stock Viande hachée
INSERT INTO Stock VALUES (1, 20);
-- Stock pâte à lasgnes
INSERT INTO Stock VALUES (2, 50);
-- Stock pain
INSERT INTO Stock VALUES (3, 30);
-- Stock avocat
INSERT INTO Stock VALUES (4, 10);

INSERT INTO Stock VALUES (5, 10);
INSERT INTO Stock VALUES (6, 9.9);
INSERT INTO Stock VALUES (7, 9.9);
INSERT INTO Stock VALUES (8, 10);
INSERT INTO Stock VALUES (9, 10);
INSERT INTO Stock VALUES (10, 10);


-- Les ingrédients

INSERT INTO Ingredient VALUES (1, 'Viande hachee', 'kg', 1);
INSERT INTO Ingredient VALUES (2, 'Pate à lasagnes', 'pièce', 2);
INSERT INTO Ingredient VALUES (3, 'Pain', 'pièce', 3);
INSERT INTO Ingredient VALUES (4, 'Avocat', 'pièce', 4);
INSERT INTO Ingredient VALUES (5, 'Pate_a_pizza', 'pièce', 5);
INSERT INTO Ingredient VALUES (6, 'sauce_tomate', 'kg', 6);
INSERT INTO Ingredient VALUES (7, 'mozzarella', 'kg', 7);
INSERT INTO Ingredient VALUES (8, 'champignon', 'kg', 10);


-- Les plats

INSERT INTO Plat VALUES (1, 'Lasagnes', 'Plat', 10.99);
INSERT INTO Plat VALUES (2, 'Avocado toast', 'Entree', 4.50);
INSERT INTO Plat VALUES (3, 'Pate carbo', 'Plat', 8.00);
INSERT INTO Plat VALUES (4, 'Ravioli au jus de pied', 'Plat', 9.00);
INSERT INTO Plat VALUES (5, 'Tiramisu roulé sous les aisselles', 'Dessert', 5.50);
INSERT INTO Plat VALUES (6, 'Hamburger veggie', 'StreetFood', 6.50);
INSERT INTO Plat VALUES (7, 'Pizza', 'StreetFood', 7.50);


-- La carte
INSERT INTO PlatCarte VALUES (1);
INSERT INTO PlatCarte VALUES (2);
INSERT INTO PlatCarte VALUES (3);
INSERT INTO PlatCarte VALUES (4);

-- Composition des plats (IdPlat, IdIngredient, Quantité) :

-- Composition des lasagnes :
	-- Viande hachée :
INSERT INTO Contient VALUES (1, 1, 0.5);
	-- Pate à lasgane
INSERT INTO Contient VALUES (1, 2, 5);
-- Composition de l'avocado toast :
	-- Pain :
INSERT INTO Contient VALUES (2, 3, 1);
	-- Avocat :
INSERT INTO Contient VALUES (2, 4, 1);

INSERT INTO Contient VALUES (7, 5, 1);
INSERT INTO Contient VALUES (7, 6, 0.1);
INSERT INTO Contient VALUES (7, 7, 0.1);
INSERT INTO Contient VALUES (7, 8, 0.1);

-- Les tables :
INSERT  INTO TableC VALUES (1, 'dressee', 4, 'serveur1', 'assistant1');
INSERT  INTO TableC VALUES (2, 'dressee', 2, 'serveur1', 'assistant1');
INSERT  INTO TableC VALUES (3, 'occupee', 5, 'serveur2', 'assistant1');
INSERT  INTO TableC VALUES (4, 'sale', 3, 'serveur2', 'assistant2');
INSERT  INTO TableC VALUES (5, 'sale', 2, 'serveur3', 'assistant3');

-- Les commandes :

INSERT INTO Commande VALUES (DEFAULT, DEFAULT, current_date, false, 2,1);
INSERT INTO Commande VALUES (DEFAULT, DEFAULT, current_date, false, 2,2);
INSERT INTO Commande VALUES (DEFAULT, DEFAULT, current_date, false, 3,3);
INSERT INTO Commande VALUES (DEFAULT, DEFAULT, current_date, false, 4,4);
