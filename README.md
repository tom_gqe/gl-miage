# PROJET Génie Logiciel MIAGE 2020-2021

* Myriam AIT HSAINE
* Tom GHESQUIERE
* Alexi JACQUET

[Lien Trello](https://trello.com/b/oXKlLfYy/gl-miage)
(le burndown chart est sur Trello)

* Le fichier script contient le script sql de la BDD
* Le fichier donnees contient le script qui contient un jeu de données
* Le fichier config.properties contient la config de la BDD