DROP TABLE Statistique;
DROP TABLE Contient;
DROP TABLE Ingredient;
DROP TABLE Commande;
DROP TABLE TableC;
DROP TABLE PlatStat;
DROP TABLE PlatCarte;
DROP TABLE Carte;
DROP TABLE Plat;
DROP TABLE Stock;
DROP TABLE Utilisateur;



CREATE TABLE Utilisateur(
	nomCompte VARCHAR(25) PRIMARY KEY NOT NULL,
	role VARCHAR(25) NOT NULL
);

CREATE TABLE Stock(
	idStock INT PRIMARY KEY NOT NULL,
	quantite REAL NOT NULL
);

CREATE TABLE Plat(
	idPlat INT PRIMARY KEY NOT NULL,
	nom VARCHAR(50) NOT NULL,
	categorie VARCHAR(50) NOT NULL,
	prix REAL NOT NULL
);

CREATE TABLE PlatCarte(
	idPlat INT NOT NULL,
	PRIMARY KEY (idPlat)
);

CREATE TABLE PlatStat(
	idPlat INT NOT NULL,
	dateStat DATE NOT NULL,
	nbCommande INT NOT NULL,
	PRIMARY KEY (idPlat, dateStat),
	FOREIGN KEY (idPlat) REFERENCES Plat(idPlat),
	FOREIGN KEY (dateStat) REFERENCES StatistiquePlat(dateStat)
);

CREATE TABLE TableC(
	numero INT PRIMARY KEY NOT NULL,
	etatOccupation VARCHAR(8) NOT NULL,
	nbCouvert INT NOT NULL,
	nomServeur VARCHAR(25) NOT NULL,
	nomAssist VARCHAR(25) NOT NULL,
	FOREIGN KEY (nomServeur) REFERENCES Utilisateur(nomCompte),
	FOREIGN KEY (nomAssist) REFERENCES Utilisateur(nomCompte)
);

CREATE TABLE Commande(
	idCommande SERIAL PRIMARY KEY NOT NULL,
	etatAvancement VARCHAR(8) DEFAULT 'attente',
	dateCommande TIMESTAMP NOT NULL,
	estPrioritaire BOOLEAN NOT NULL,
	numeroTable INT NOT NULL,
	idPlat INT NOT NULL,
	FOREIGN KEY (numeroTable) REFERENCES TableC(numero),
	FOREIGN KEY (idPlat) REFERENCES Plat(idPlat)

);

CREATE TABLE Ingredient(
	idIngredient  INT PRIMARY KEY NOT NULL,
	nom VARCHAR(25) NOT NULL,
	unite VARCHAR(25) NOT NULL,
	idStock INT NOT NULL,
	FOREIGN KEY (idStock) REFERENCES Stock(idStock)
);

CREATE TABLE Contient(
	idPlat INT NOT NULL,
	idIngredient INT NOT NULL,
	quantite REAL NOT NULL,
	PRIMARY KEY (idPlat, idIngredient),
	FOREIGN KEY (idPlat) REFERENCES Plat(idPlat),
	FOREIGN KEY (idIngredient) REFERENCES Ingredient(idIngredient)
);

CREATE TABLE Statistique(
	dateStat DATE,
	tempsPrepTota INT,
	nbPrepTotal INT,
	tempsClientTotal INT,
	nbClientTotal INT,
	recetteDej REAL,
	recetteDiner REAL
);

ALTER TABLE Utilisateur
ADD CONSTRAINT chk_role CHECK (role in ('serveur','assistant','directeur','cuisinier','maitre'));

ALTER TABLE TableC
ADD CONSTRAINT chk_etat CHECK (etatOccupation in ('dressee','occupee','sale','nettoyee'));
