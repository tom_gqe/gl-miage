package modeles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import application.Main;

public class Statistique {

	private LocalDate date;
	private int tempsTotalPreparation; // temps en secondes
	private int nbPreparationTotal;
	private int tempsTotalClient; // temps en secondes
	private int nbClientTotal;
	private double recetteDiner;
	private double recetteDejeuner;

	public Statistique() throws SQLException {
		this.date = LocalDate.now();
		this.tempsTotalPreparation = 0;
		this.tempsTotalClient = 0;
		this.nbClientTotal = 0;
		this.nbPreparationTotal = 0;
		this.recetteDejeuner = 0;
		this.recetteDiner = 0;
		// appel bdd pour créer
		insertBDD();
		// date = Date.valueOf(dateDuJour);
		// Time t = Time.valueOf(LocalTime.of(0, 0, tempsTotalPreparation, 0));
	}
	
	public Statistique(LocalDate date, int tempsTotalPreparation, int nbPreparationTotal, int tempsTotalClient,
			int nbClientTotal, double recetteDiner, double recetteDejeuner) {
		this.date = date;
		this.tempsTotalPreparation = tempsTotalPreparation;
		this.nbPreparationTotal = nbPreparationTotal;
		this.tempsTotalClient = tempsTotalClient;
		this.nbClientTotal = nbClientTotal;
		this.recetteDiner = recetteDiner;
		this.recetteDejeuner = recetteDejeuner;
	}

	

	/**
	 * méthode permettant de préparer la commande
	 * @param tempsDePreparation
	 * @throws SQLException
	 */
	public static void commandePrepareeNow(int tempsDePreparation) throws SQLException {
		Statistique stat = findStatistique(LocalDate.now());
		if (stat == null) {
			stat = new Statistique();
		}
		stat.commandePreparee(tempsDePreparation);
		stat.updateBDD();
	}
	
	/**
	 * méthode permettant d'incrémenter les stats de temps de préparation de la commande
	 * @param tempsDePreparation
	 */
	public void commandePreparee(int tempsDePreparation) {
		this.tempsTotalPreparation += tempsDePreparation;
		this.nbPreparationTotal++;
	}
	
	/**
	 * méthode permettant de rédiger la facture de la commande d'un client
	 * @param tempsATable
	 * @param montantPaye
	 * @param dejeuner
	 * @throws Exception
	 */
	public static void clientFactureNow(int tempsATable, double montantPaye, boolean dejeuner) throws Exception {
		Statistique stat = findStatistique(LocalDate.now());
		if (stat == null) {
			stat = new Statistique();
		}
		stat.clientFacture(tempsATable, montantPaye, dejeuner);
		stat.updateBDD();
	}

	/**
	 * méthode permettant d'incrémenter les stats de recette
	 * @param tempsATable
	 * @param montantPaye
	 * @param dejeuner
	 * @throws Exception
	 */
	public void clientFacture(int tempsATable, double montantPaye, boolean dejeuner) throws Exception {
		this.tempsTotalClient += tempsATable;
		if (dejeuner) {
			this.recetteDejeuner += montantPaye;
		} else {
			this.recetteDiner += montantPaye;
		}
	}
	
	/**
	 * méthode permettant de gérer les stats de nombre de client
	 * @param nbClient
	 * @throws SQLException
	 */
	public static void clientInstalleNow(int nbClient) throws SQLException {
		Statistique stat = findStatistique(LocalDate.now());
		if (stat == null) {
			stat = new Statistique();
		}
		stat.clientInstalle(nbClient);
		stat.updateBDD();
	}
	
	/**
	 * méthode permettant d'incrémenter le nombre de client
	 * @param nbClient
	 * @throws SQLException
	 */
	public void clientInstalle(int nbClient) throws SQLException {
		this.nbClientTotal += nbClient;
	}

	/**
	 * méthode permettant de mettre à jour la BDD
	 * @throws SQLException
	 */
	private void updateBDD() throws SQLException {
		String SQL = "UPDATE statistique SET tempspreptota=?, nbPrepTotal=?, tempsClientTotal=?, nbClientTotal=?, recetteDej=?, recetteDiner = ? WHERE datestat = ?";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1, this.tempsTotalPreparation);
		pstmt.setInt(2, this.nbPreparationTotal);
		pstmt.setInt(3, this.tempsTotalClient);
		pstmt.setInt(4, this.nbClientTotal);
		pstmt.setDouble(5, this.recetteDejeuner);
		pstmt.setDouble(6, this.recetteDiner);
		pstmt.setDate(7, java.sql.Date.valueOf(this.date));

		pstmt.executeUpdate();
	}
	
	/**
	 * méthode permettant d'ajouter dans la BDD les stats
	 * @throws SQLException
	 */
	public void insertBDD() throws SQLException {

		String SQL = "INSERT INTO statistique VALUES (?, ?, ?, ?, ?,?, ?)";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(2, this.tempsTotalPreparation);
		pstmt.setInt(3, this.nbPreparationTotal);
		pstmt.setInt(4, this.tempsTotalClient);
		pstmt.setInt(5, this.nbClientTotal);
		pstmt.setDouble(6, this.recetteDejeuner);
		pstmt.setDouble(7, this.recetteDiner);
		pstmt.setDate(1, java.sql.Date.valueOf(this.date));

		pstmt.executeUpdate();
	}

	public int getTempsPreparationMoyen() {
		return this.tempsTotalPreparation / this.nbPreparationTotal;
	}

	public int getTempsRotationMoyen() {
		return this.tempsTotalClient / this.getNbClientTotal();
	}

	/**
	 * méthode permettant de récupérer les 5 plats les plus populaires
	 * @return
	 * @throws SQLException
	 */
	public static List<String> get5PlatPopulaire() throws SQLException {
		List<String> liste_nom_plats = new ArrayList<String>();

		String requete = "select plat.idplat, plat.nom from platstat inner join plat on plat.idplat = platstat.idplat group by plat.idplat order by sum(platstat.nbcommande) desc limit 5;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		while (rs.next()) {
			liste_nom_plats.add(rs.getString(2));
		}
		return liste_nom_plats;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public int getTempsTotalPreparation() {
		return tempsTotalPreparation;
	}

	public void setTempsTotalPreparation(int tempsTotalPreparation) {
		this.tempsTotalPreparation = tempsTotalPreparation;
	}

	public int getNbPreparationTotal() {
		return nbPreparationTotal;
	}

	public void setNbPreparationTotal(int nbPreparationTotal) {
		this.nbPreparationTotal = nbPreparationTotal;
	}

	public int getTempsTotalClient() {
		return tempsTotalClient;
	}

	public void setTempsTotalClient(int tempsTotalClient) {
		this.tempsTotalClient = tempsTotalClient;
	}

	public int getNbClientTotal() {
		return nbClientTotal;
	}

	public void setNbClientTotal(int nbClientTotal) {
		this.nbClientTotal = nbClientTotal;
	}

	public double getRecetteDiner() {
		return recetteDiner;
	}

	public void setRecetteDiner(double recetteDiner) {
		this.recetteDiner = recetteDiner;
	}

	public double getRecetteDejeuner() {
		return recetteDejeuner;
	}

	public void setRecetteDejeuner(double recetteDejeuner) {
		this.recetteDejeuner = recetteDejeuner;
	}

	/**
	 * méthode permettant de récupérer les statistiques en fonction d'une date
	 * @param date
	 * @return
	 * @throws SQLException
	 */
	public static Statistique findStatistique(LocalDate date) throws SQLException {
		Statistique stat = null;
		String SQL = "SELECT * FROM statistique WHERE datestat = ?";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setDate(1, java.sql.Date.valueOf(date));
		
		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			stat = new Statistique(date, rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getDouble(6), rs.getDouble(7));
		}
		return stat;
	}
}
