package modeles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

import application.Main;

public class Commande {

	public static final String ATTENTE = "attente";
	public static final String PREPAREE = "preparee";
	public static final String FACTUREE = "facturee";
	public static final String SERVIE = "servie";

	private Plat plat;
	private Table table;
	private String etat;
	private boolean estPrioritaire;
	private LocalDateTime date;
	private int id;

	public Commande(int id, Plat plat, Table table, String etat, boolean estPrioritaire, LocalDateTime date) {
		this.id = id;
		this.plat = plat;
		this.table = table;
		this.etat = etat;
		this.estPrioritaire = estPrioritaire;
		this.date = date;
	}

	public Commande(Plat plat, Table table, String etat, boolean estPrioritaire, LocalDateTime date) {
		this.plat = plat;
		this.table = table;
		this.etat = etat;
		this.estPrioritaire = estPrioritaire;
		this.date = date;
	}

	public Commande(int id) throws SQLException {
		Commande c = getCommandeById(id);
		this.id = id;
		this.plat = c.getPlat();
		this.table = c.getTable();
		this.etat = c.getEtat();
		this.estPrioritaire = c.getPrioritaire();
		this.date = c.getDate();
	}

	/**
	 * méthode permettant de préparer un plat, cela met à jour les stocks
	 * @throws Exception
	 */
	public void preparer() throws Exception {
		if(this.plat.estCuisinable()) {
			this.plat.cuisiner();
			this.etat = Commande.PREPAREE;
			Statistique.commandePrepareeNow((int) Duration.between(this.date, LocalDateTime.now()).toSeconds());
			this.updateBDD();
		}else {
			System.out.println("La commande ne peut pas être préparer car il n'y a pas assez d'ingrédients");
		}
	}

	/**
	 * méthode permettant de payer la commande
	 * @throws Exception
	 */
	public void payer() throws Exception {
		if (this.etat != Commande.PREPAREE) {
			System.out.println("Un des plats n'est pas prêt");
		} else {
			this.etat = Commande.FACTUREE;
		}
		this.updateBDD();
	}

	/**
	 * méthode permettant de servir la commande
	 * @throws Exception
	 */
	public void servir() throws Exception {
		this.etat = Commande.SERVIE;
		this.updateBDD();
	}

	public double getPrix() {
		return this.plat.getPrix();
	}

	public String getEtat() {
		return this.etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public Boolean getPrioritaire() {
		return this.estPrioritaire;
	}

	public Plat getPlat() {
		return this.plat;
	}

	public Table getTable() {
		return this.table;
	}

	public LocalDateTime getDate() {
		return this.date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPlat(Plat plat) {
		this.plat = plat;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	/**
	 * méthode permettant de récupérer le prochain ID à insérer dans la BDD
	 * @return
	 * @throws SQLException
	 */
	public static int getMaxId() throws SQLException {
		String requete = "SELECT MAX(idcommande) FROM commande;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		rs.next();
		return rs.getInt(1)+1;
	}

	/**
	 * méthode permettant d'ajouter la commande dans la BDD
	 * @param commande
	 * @throws SQLException
	 */
	public static void insertBDD(Commande commande) throws SQLException {
		commande.setId(getMaxId());

		String SQL = "INSERT INTO Commande VALUES (DEFAULT, DEFAULT, current_timestamp, ?, ?,?)";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setBoolean(1, commande.getPrioritaire());
		pstmt.setInt(2, commande.getTable().getNumero());
		pstmt.setInt(3, commande.getPlat().getId());

		pstmt.executeUpdate();
	}

	/**
	 * méthode permettant de récupérer la liste des commandes en attentes
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Commande> listCommandeAttente() throws SQLException {
		ArrayList<Commande> res = new ArrayList<Commande>();
		String SQL = "SELECT * FROM commande WHERE etatavancement = ? AND estprioritaire = true ORDER BY datecommande, idcommande ASC";
		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setString(1, Commande.ATTENTE);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			res.add(getCommandeById(rs.getInt("idcommande")));
		}

		SQL = "SELECT * FROM commande WHERE etatavancement = ? AND estprioritaire = false ORDER BY datecommande, idcommande ASC";
		pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setString(1, Commande.ATTENTE);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			res.add(getCommandeById(rs.getInt("idcommande")));
		}
		return res;
	}

	/**
	 * méthode permettant de récuépérer une commande par son ID
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static Commande getCommandeById(int id) throws SQLException {
		Commande commande = null;
		Plat plat = null;
		Table table = null;
		int idPlat = 0;
		int numeroTable = 0;

		String SQL = "SELECT * FROM commande WHERE idcommande = ?";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1, id);

		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			commande = new Commande(id, plat, table, rs.getString("etatavancement"), rs.getBoolean("estprioritaire"),
					rs.getTimestamp("datecommande").toLocalDateTime());
			idPlat = rs.getInt("idplat");
			numeroTable = rs.getInt("numerotable");
		}

		String SQL2 = "SELECT nom FROM plat WHERE idplat = ?";

		PreparedStatement pstmt2 = Main.connection.prepareStatement(SQL2);
		pstmt2.setInt(1, idPlat);

		ResultSet rs2 = pstmt2.executeQuery();
		if (rs2.next()) {
			plat = Plat.findPlat(rs2.getString("nom"));
		}

		String SQL3 = "SELECT * FROM tablec WHERE numero = ?";

		PreparedStatement pstmt3 = Main.connection.prepareStatement(SQL3);
		pstmt3.setInt(1, numeroTable);

		ResultSet rs3 = pstmt3.executeQuery();
		if (rs3.next()) {
			table = new Table(rs3.getInt("numero"));
		}
		commande.setPlat(plat);
		commande.setTable(table);
		return commande;
	}

	/**
	 * méthode permettant de mettre à jour la commande dans la BDD
	 * @throws Exception
	 */
	private void updateBDD() throws Exception {
		Main.connexionBDD();
		String SQL = "UPDATE commande SET etatavancement=?, datecommande=?, estprioritaire=?, numerotable=?, idplat=? WHERE idcommande = ?";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setString(1, this.etat);
		pstmt.setTimestamp(2, java.sql.Timestamp.valueOf(date));
		pstmt.setBoolean(3, this.estPrioritaire);
		pstmt.setInt(4, this.table.getNumero());
		pstmt.setInt(5, this.plat.getId());
		pstmt.setInt(6, this.id);

		pstmt.executeUpdate();
	}
	
	
	/**
	 * méthode permettant de récupérer la date de la commande
	 * @param numerotable
	 * @return
	 * @throws SQLException
	 */
	public static LocalDateTime getDateCommandeBytable(int numerotable) throws SQLException {
		LocalDateTime dateres = LocalDateTime.now();

		String SQL = "SELECT * FROM commande WHERE numerotable = ? ORDER BY datecommande ASC";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1, numerotable);

		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			dateres = rs.getTimestamp("datecommande").toLocalDateTime();
		}
		return dateres;
	}

}
