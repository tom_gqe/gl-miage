package modeles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import application.Main;

public class Plat {

	private int id;
	private String nom;
	private String categorie;
	private double prix;
	private ArrayList<Ingredient> ingredients;
	private ArrayList<Double> quantites;


	public Plat(String nom, String categorie, double prix, ArrayList<Ingredient> ingredients,
			ArrayList<Double> quantites) throws SQLException {
		this.nom = nom;
		this.categorie = categorie;
		this.prix = prix;
		this.ingredients = ingredients;
		this.quantites = quantites;
	}

	public Plat() {
	}
	
	public Plat(String nom) throws SQLException {
		Plat p = findPlat(nom);
		this.id = p.getId();
		this.nom = p.getNom();
		this.categorie = p.getCategorie();
		this.prix = p.getPrix();
		this.ingredients = p.getIngredients();
		this.quantites = p.getQuantites();
	}

	/**
	 * Méthode permettant d'ajouter un nouvel ingrédient au plat Si le plat existe
	 * déjà, la méthode va modifier la quantité de l'ingrédient necessaire à la
	 * réalisation du plat
	 * 
	 * @param ingredient
	 * @param qte
	 * @throws Exception
	 */
	public void ajouterIngredient(Ingredient ingredient, double qte) throws Exception {
		int index = -1;
		for (int i = 0; i < this.ingredients.size(); i++) {
			if (this.ingredients.get(i).getNom().equals(ingredient.getNom())) {
				index = i;
			}
		}
		if (index == -1) {
			this.ingredients.add(ingredient);
			this.quantites.add(qte);
		} else {
			this.quantites.set(index, qte);
		}
		this.updateBDD();
	}

	/**
	 * Méthode permettant de lancer la cuisson du plat et donc de mettre à jour les
	 * stocks
	 * 
	 * @throws Exception
	 */
	public void cuisiner() throws Exception {
		if (this.estCuisinable()) {
			// màj des stocks
			for (int i = 0; i < this.ingredients.size(); i++) {
				this.ingredients.get(i).utiliser(this.quantites.get(i));
			}
		} else {
			System.out.println("pas assez d'ingrédients");
		}
	}

	/**
	 * Méthode permettant de retirer un ingrédient au plat
	 * 
	 * @param ingredient
	 * @param qte
	 * @throws Exception
	 */
	public void retirerIngredient(Ingredient ingredient) throws Exception {
		int index = -1;
		for (int i = 0; i < this.ingredients.size(); i++) {
			if (this.ingredients.get(i).getNom().equals(ingredient.getNom())) {
				index = i;
			}
		}
		if (index == -1) {
			System.out.println("L'ingrédient n'existe pas");
		} else {
			this.ingredients.remove(ingredient);
			this.quantites.remove(index);
		}
		this.updateBDD();
	}

	/**
	 * méthode permettant de mettre à jour la BDD
	 * @throws Exception
	 */
	private void updateBDD() throws Exception {
		Main.connexionBDD();
		String SQL = "UPDATE plat SET nom=?, categorie=?, prix=? WHERE idplat = ?";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setString(1, this.nom);
		pstmt.setString(2, this.categorie);
		pstmt.setDouble(3, this.prix);
		pstmt.setInt(4, this.id);

		pstmt.executeUpdate();

		for (int i = 0; i < this.ingredients.size(); i++) {

			SQL = "DELETE FROM contient WHERE idplat = ? AND idingredient = ?";

			PreparedStatement pstmt2 = Main.connection.prepareStatement(SQL);
			pstmt2.setInt(1, this.id);
			pstmt2.setInt(2, this.ingredients.get(i).getId());

			pstmt2.executeUpdate();
		}

		for (int i = 0; i < this.ingredients.size(); i++) {
			SQL = "INSERT INTO contient VALUES (?, ?, ?)";

			PreparedStatement pstmt2 = Main.connection.prepareStatement(SQL);
			pstmt2.setInt(1, this.id);
			pstmt2.setInt(2, this.ingredients.get(i).getId());
			pstmt2.setDouble(3, this.quantites.get(i));

			pstmt2.executeUpdate();
		}
	}

	/**
	 * méthode permettant de savoir si le plat est cuisinable ou non en fonction des stocks
	 * @return
	 */
	public boolean estCuisinable() {
		boolean estCuisinable = true;
		for (int i = 0; i < this.ingredients.size(); i++) {
			try {
				Stock stock = new Stock(this.ingredients.get(i));
				if (this.quantites.get(i) > stock.getQuantite() && estCuisinable) {
					estCuisinable = false;
				} else {
					estCuisinable = true;
				}
			} catch (SQLException e) {
			}

		}
		return estCuisinable;
	}

	/**
	 * méthode permettant de récupérér la liste des plats existatns
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<String> getListePlatExistant() throws SQLException {
		ArrayList<String> liste_plat_existant = new ArrayList<String>();
		String requete = "SELECT nom FROM plat;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		while (rs.next()) {
			liste_plat_existant.add(rs.getString("nom"));
		}
		return liste_plat_existant;
	}

	/**
	 * méthode permettant d'ajouter un plat dans la BDD
	 * @param plat
	 * @throws SQLException
	 */
	public static void insertPlatBDD(Plat plat) throws SQLException {
		int id = getMaxId();
		ArrayList<String> liste_plat_existant = getListePlatExistant();
		if (!liste_plat_existant.contains(plat.getNom())) {
			String SQL = "INSERT INTO plat VALUES (?, ?, ?, ?)";

			PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
			pstmt.setInt(1, id);
			pstmt.setString(2, plat.getNom());
			pstmt.setString(3, plat.getCategorie());
			pstmt.setDouble(4, plat.getPrix());

			pstmt.executeUpdate();
			for (int i = 0; i < plat.getIngredients().size(); i++) {
				SQL = "INSERT INTO contient VALUES (?, ?, ?)";

				pstmt = Main.connection.prepareStatement(SQL);
				pstmt.setInt(1, id);
				pstmt.setInt(2, plat.getIngredients().get(i).getId());
				pstmt.setDouble(3, plat.getQuantites().get(i));

				pstmt.executeUpdate();
			}
		} else {
			String SQL = "SELECT idplat FROM plat WHERE nom = ?";

			PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
			pstmt.setString(1, plat.getNom());

			ResultSet rs = pstmt.executeQuery();

			rs.next();

			id = rs.getInt(1);
		}
		plat.setId(id);
	}

	public double getPrix() {
		return this.prix;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public ArrayList<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(ArrayList<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public ArrayList<Double> getQuantites() {
		return quantites;
	}

	public void setQuantites(ArrayList<Double> quantites) {
		this.quantites = quantites;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	/**
	 * méthode permettant d'avoir la liste des catégories des plats de la carte
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<String> listCategories() throws SQLException {
		ArrayList<String> listeCategorie = new ArrayList<String>();
		String requete = "SELECT DISTINCT plat.categorie FROM plat INNER JOIN platcarte ON plat.idplat = platcarte.idplat;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		while (rs.next()) {
			listeCategorie.add(rs.getString("categorie"));
		}
		return listeCategorie;
	}

	/**
	 * méthode permettant d'avoir la liste des plats par la catégorie choisie, des plats de la carte
	 * @param categorieChoisie
	 * @param cuisinableUniquement
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Plat> listPlatByCategorie(String categorieChoisie, boolean cuisinableUniquement) throws SQLException {
		ArrayList<Plat> liste_plat_existant = new ArrayList<Plat>();
		String requete = "SELECT plat.nom FROM plat INNER JOIN platcarte ON plat.idplat = platcarte.idplat WHERE plat.categorie = ?;";
		PreparedStatement stmt = Main.connection.prepareStatement(requete);
		stmt.setString(1, categorieChoisie);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			liste_plat_existant.add(findPlat(rs.getString("nom")));
		}
		return liste_plat_existant;
	}
	
	public static ArrayList<Plat> listPlatCarte() throws SQLException {
		ArrayList<Plat> liste_plat_existant = new ArrayList<Plat>();
		String requete = "SELECT plat.nom FROM plat INNER JOIN platcarte ON plat.idplat = platcarte.idplat;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		while (rs.next()) {
			liste_plat_existant.add(findPlat(rs.getString("nom")));
		}
		return liste_plat_existant;
	}
	
	public static ArrayList<Plat> listPlat() throws SQLException {
		ArrayList<Plat> liste_plat_existant = new ArrayList<Plat>();
		String requete = "SELECT plat.nom FROM plat;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		while (rs.next()) {
			liste_plat_existant.add(findPlat(rs.getString("nom")));
		}
		return liste_plat_existant;
	}

	/**
	 * méthode permettant de récupérer un plat par son nom
	 * @param nom
	 * @return
	 * @throws SQLException
	 */
	public static Plat findPlat(String nom) throws SQLException {
		Plat plat = null;
		String requete = "SELECT * FROM plat WHERE nom = ?;";
		PreparedStatement stmt = Main.connection.prepareStatement(requete);
		stmt.setString(1, nom);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			plat = new Plat();
			plat.setId(rs.getInt(1));
			plat.setNom(rs.getString(2));
			plat.setCategorie(rs.getString(3));
			plat.setPrix(rs.getDouble(4));
			String requete2 = "SELECT * FROM Contient WHERE IdPlat = ?;";
			PreparedStatement stmt2 = Main.connection.prepareStatement(requete2);
			stmt2.setInt(1, plat.getId());
			ResultSet rs2 = stmt2.executeQuery();
			ArrayList<Ingredient> ingr = new ArrayList<Ingredient>();
			ArrayList<Double> qte = new ArrayList<Double>();
			while (rs2.next()) {
				ingr.add(Ingredient.getIngredientById(rs2.getInt("IdIngredient")));
				qte.add(rs2.getDouble("quantite"));
			}
			plat.setIngredients(ingr);
			plat.setQuantites(qte);

		}
		return plat;
	}
	
	/**
	 * méthode permettant de récupérer le prochain ID à insérer dans la BDD
	 * @return
	 * @throws SQLException
	 */
	public static int getMaxId() throws SQLException {
		String requete = "SELECT MAX(idplat) FROM plat;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		rs.next();
		return rs.getInt(1)+1;	
	}

}
