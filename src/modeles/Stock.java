package modeles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import application.Main;

public class Stock {

	private int idStock;
	private Ingredient ingredient;
	private double quantite;

	public Stock(Ingredient ingredient) throws SQLException {
		this.ingredient = ingredient;
		this.idStock = getIdStockBDD(ingredient);
		this.quantite = Stock.getQuantiteBDD(this.idStock);
	}

	public Stock(int idStock) {
		this.idStock = idStock;
	}

	/**
	 * méthode permettant d'ajouter une quantité d'ingredient
	 * @param qte
	 * @throws Exception
	 */
	public void ajouter(double qte) throws Exception {
		if(qte > 0) {
			this.quantite += qte;
			updateBDD();
		}
	}

	/**
	 * méthode permettant de retirer une quantite d'ingredient
	 * @param qte
	 * @throws Exception
	 */
	public void retirer(double qte) throws Exception {
		if(qte > 0) {
			if(this.quantite-qte < 0) {
				this.quantite = 0;
			}else {
				this.quantite -= qte;
			}
			updateBDD();
		}
	}

	public String afficher() {
		String res = this.quantite + this.ingredient.getUnite() + this.ingredient.getNom();
		return res;
	}

	/**
	 * méthode permettant de mettre à jour la BDD
	 * @throws Exception
	 */
	private void updateBDD() throws Exception {
		Main.connexionBDD();
		String SQL = "UPDATE stock SET quantite=? WHERE idstock = ?";

		PreparedStatement pstmt2 = Main.connection.prepareStatement(SQL);
		pstmt2.setDouble(1, this.quantite);
		pstmt2.setInt(2, this.idStock);

		pstmt2.executeUpdate();
	}

	/**
	 * méthode permettant de récupérer l'id du stock d'un ingredient
	 * @param ingredient
	 * @return
	 * @throws SQLException
	 */
	public static Integer getIdStockBDD(Ingredient ingredient) throws SQLException {
		int result = 0;
		String SQL = "SELECT idstock FROM ingredient WHERE idingredient = ?";
		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1, ingredient.getId());
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			result = rs.getInt(1);
		}
		return result;
	}

	/**
	 * méthode permettant de récupérer la quantité d'ingredient de la BDD
	 * @return
	 * @throws SQLException
	 */
	public double getQuantiteBDD() throws SQLException {
		String SQL = "SELECT quantite FROM stock WHERE idstock = ?";
		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1, this.idStock);
		ResultSet rs = pstmt.executeQuery();
		rs.next();
		double result = rs.getDouble(1);
		return result;
	}
	
	/**
	 * méthode permettant de récupérer la quantité d'ingredient de la BDD avec un id en parametre
	 * @param idStock
	 * @return
	 * @throws SQLException
	 */
	public static double getQuantiteBDD(int idStock) throws SQLException {
		double result = 0.0;
		String SQL = "SELECT quantite FROM stock WHERE idstock = ?";
		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1,idStock);
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			result = rs.getDouble(1);
		}
		return result;
	}

	public double getQuantite() {
		return quantite;
	}

	public void setQuantite(double quantite) throws Exception {
		this.quantite = quantite;
		updateBDD();
	}

	/**
	 * méthode permettant de récupérer le prochain ID à insérer dans la BDD
	 * @return
	 * @throws SQLException
	 */
	public static int getMaxId() throws SQLException {
		String requete = "SELECT MAX(idstock) FROM stock;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		rs.next();
		return rs.getInt(1)+1;	
	}

}
