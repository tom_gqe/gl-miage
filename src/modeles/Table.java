package modeles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import application.Main;

public class Table {

	public static final String DRESSEE = "dressee";
	public static final String OCCUPEE = "occupee";
	public static final String SALE = "sale";
	public static final String NETTOYEE = "nettoyee";

	private int numero;
	private String etat;
	private int nbCouverts;
	private Utilisateur serveur;
	private Utilisateur assistant;

	public Table(int numero, int nbCouverts) throws SQLException {
		this.numero = numero;
		this.nbCouverts = nbCouverts;
		this.etat = Table.NETTOYEE;
	}

	public Table(int numero, int nbCouverts, String etat, Utilisateur serveur, Utilisateur assistant) {
		this.numero = numero;
		this.nbCouverts = nbCouverts;
		this.etat = etat;
		this.serveur = serveur;
		this.assistant = assistant;
	}

	public Table(int numero) throws SQLException {
		Table t = findTable(numero);
		this.numero = numero;
		this.etat = t.getEtat();
		this.nbCouverts = t.getNbCouverts();
		this.serveur = t.getServeur();
		this.assistant = t.getAssistant();
	}

	/**
	 * méthode permettant de lister les tables de l'utilisateur en parametre
	 * @param utilisateur
	 * @return
	 * @throws SQLException
	 */
	public static List<Table> listTable(Utilisateur utilisateur) throws SQLException {
		ArrayList<Table> res = new ArrayList<Table>();
		String SQL = "SELECT * FROM TableC";
		PreparedStatement pstmt;
		switch (utilisateur.getRole()) {
		case Utilisateur.ASSISTANT_SERVICE:
			System.out.println("bbb");
			SQL += " WHERE nomAssist = ?";
			pstmt = Main.connection.prepareStatement(SQL);
			pstmt.setString(1, utilisateur.getNom());
			break;
		case Utilisateur.SERVEUR:
			SQL += " WHERE nomServeur = ?";
			pstmt = Main.connection.prepareStatement(SQL);
			pstmt.setString(1, utilisateur.getNom());
			break;
		default:
			pstmt = Main.connection.prepareStatement(SQL);
		}
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			res.add(new Table(rs.getInt("numero"), rs.getInt("nbCouvert"), rs.getString("etatOccupation"),
					Utilisateur.findUtilisateur(rs.getString("nomServeur"), Main.connection),
					Utilisateur.findUtilisateur(rs.getString("nomAssist"), Main.connection)));
		}
		return res;
	}

	/**
	 * méthode permettant de liste les tables occupées
	 * @return
	 * @throws SQLException
	 */
	public static List<Table> listTableOccupee() throws SQLException {
		ArrayList<Table> res = new ArrayList<Table>();

		String SQL = "SELECT * FROM TableC WHERE etatoccupation = 'occupee'";
		PreparedStatement pstmt;
		pstmt = Main.connection.prepareStatement(SQL);

		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			res.add(new Table(rs.getInt("numero"), rs.getInt("nbCouvert"), rs.getString("etatOccupation"),
					Utilisateur.findUtilisateur(rs.getString("nomServeur"), Main.connection),
					Utilisateur.findUtilisateur(rs.getString("nomAssist"), Main.connection)));
		}

		return res;
	}

	/**
	 * méthode permettant de récupérer les informations d'une table depuis la BDD à partir de son numéro
	 * @param numero
	 * @return
	 * @throws SQLException
	 */
	public static Table findTable(int numero) throws SQLException {
		Table res = null;
		String SQL = "SELECT * FROM TableC WHERE numero = ?";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1, numero);

		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			res = new Table(rs.getInt("numero"), rs.getInt("nbCouvert"), rs.getString("etatOccupation"),
					Utilisateur.findUtilisateur(rs.getString("nomServeur"), Main.connection),
					Utilisateur.findUtilisateur(rs.getString("nomAssist"), Main.connection));
		}

		return res;
	}

	/**
	 * méthode permettant d'assigner des clients à une table
	 * @param nbClient
	 * @return
	 * @throws Exception
	 */
	public boolean assignerClient(int nbClient) throws Exception {
		boolean possible = false;
		if (this.etat.equals(Table.DRESSEE) && nbClient > 0 && nbClient <= this.nbCouverts) {
			this.etat = Table.OCCUPEE;
			possible = true;
			this.updateBDD();
			Statistique.clientInstalleNow(nbClient);
		}
		return possible;
	}

	/**
	 * méthode permettant d'assigner un serveur à une table
	 * @param utilisateur
	 * @throws Exception
	 */
	public void assignerServeur(Utilisateur utilisateur) throws Exception {
		this.serveur = utilisateur;
		this.updateBDD();
	}

	/**
	 * méthode permettant d'assigner un assistant à une table
	 * @param utilisateur
	 * @throws Exception
	 */
	public void assignerAssistant(Utilisateur utilisateur) throws Exception {
		this.assistant = utilisateur;
		this.updateBDD();
	}

	/**
	 * méthode permettant de changer l'état d'une table
	 * @throws Exception
	 */
	public void changerEtatTable() throws Exception {

		switch (this.getEtat()) {
		case Table.NETTOYEE:
			System.out.println("La table a été dressée");
			this.dresser();
			break;
		case Table.SALE:
			System.out.println("La table a été nettoyée");
			this.nettoyer();
			break;
		}
		this.updateBDD();
	}

	/**
	 * méthode permettant de valider la commande d'une table
	 * @param commande
	 * @return
	 * @throws SQLException
	 */
	public boolean validerCommmande(List<Commande> commande) throws SQLException {
		if (commande.isEmpty()) {
			System.out.println("Cette commande est vide");
			return false;
		} else {
			for (Commande commande2 : commande) {
				Commande.insertBDD(commande2);
			}
			return true;
		}
	}

	/**
	 * méthode permettant d'éditer la facture d'une table
	 * @return
	 * @throws Exception
	 */
	public double editerFacture() throws Exception {
		double addition = 0;

		Main.connexionBDD();
		String SQL = "select sum(prix) as sumprix from plat inner join commande on plat.idplat = commande.idplat where commande.numerotable = ? AND etatavancement = 'attente'";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1, this.numero);

		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			addition = rs.getDouble("sumprix");
		}
		
		this.liberer();
		this.updateBDD();

		return addition;
	}
	
	/**
	 * méthode permettant de payer la commande d'une table
	 * @throws Exception
	 */
	public void payerCommande() throws Exception {

		Main.connexionBDD();
		String SQL = "select idcommande from commande where commande.numerotable = ? AND etatavancement = 'attente'";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1, this.numero);

		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			Commande.getCommandeById(rs.getInt(1)).payer();
		}
	}

	/**
	 * méthode permettant de liberer une table, lorsqu'elle passe de l'état occupée à l'état sale
	 * @return
	 */
	public boolean liberer() {
		if (!etat.equals(Table.OCCUPEE)) {
			return false;
		}
		this.etat = Table.SALE;
		return true;
	}

	/**
	 * méthode permettant de nettoyer la table
	 * @return
	 */
	public boolean nettoyer() {
		if (!etat.equals(Table.SALE)) {
			return false;
		}
		this.etat = Table.NETTOYEE;
		return true;
	}

	/**
	 * méthode permettant de dresser la table
	 * @return
	 */
	public boolean dresser() {
		if (!etat.equals(Table.NETTOYEE)) {
			return false;
		}
		this.etat = Table.DRESSEE;
		return true;
	}

	/**
	 * méthode permettant de mettre à jour la BDD
	 * @throws Exception
	 */
	private void updateBDD() throws Exception {
		Main.connexionBDD();
		String SQL = "UPDATE tablec SET etatoccupation=?, nbcouvert=?, nomserveur=?, nomassist=? WHERE numero = ?";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setString(1, this.etat);
		pstmt.setInt(2, this.nbCouverts);
		pstmt.setString(3, this.serveur.getNom());
		pstmt.setString(4, this.assistant.getNom());

		pstmt.setInt(5, this.numero);

		pstmt.executeUpdate();
	}

	public String getEtat() {
		return this.etat;
	}

	public int getNumero() {
		return this.numero;
	}

	public int getNbCouverts() {
		return nbCouverts;
	}

	public Utilisateur getServeur() {
		return serveur;
	}

	public Utilisateur getAssistant() {
		return assistant;
	}

	@Override
	public String toString() {
		return "Table numero : " + numero + "\n" + etat + "\n" + nbCouverts + " couverts" + "\n serveur : "
				+ serveur.getNom() + "\n assistant de service : " + assistant.getNom();
	}

}
