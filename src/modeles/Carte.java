package modeles;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import application.Main;

/**
 * @author Megaport
 *
 */
public class Carte {

	private List<Plat> plats;

	public Carte() throws SQLException {
		this.plats = Plat.listPlatCarte();
	}
	/**
	 * méthode permettant d'ajouter un plat à la carte
	 * @param plat
	 * @return
	 * @throws Exception 
	 */
	// retourne true si ajouté
	// retourne false si déjà dans la carte
	public boolean ajouterPlat(Plat plat) throws Exception {
		boolean trouve = false;
		ArrayList<String> liste_nom_plat = new ArrayList<String>();
		for(Plat p: this.plats) {
			liste_nom_plat.add(p.getNom());
		}
		if (!liste_nom_plat.contains(plat.getNom())) {
			this.plats.add(plat);
			trouve = true;
		}
		return trouve;
	}
	
	/**
	 * méthode permettant de retirer un plat de la carte
	 * @param plat
	 * @return
	 * @throws Exception 
	 */
	public boolean retirerPlat(Plat plat) throws Exception {
		boolean trouve = false;
		ArrayList<String> liste_nom_plat = new ArrayList<String>();
		for(Plat p: this.plats) {
			liste_nom_plat.add(p.getNom());
		}
		if (liste_nom_plat.contains(plat.getNom())) {
			int index = 0;
			int i = 0;
			for(Plat p: this.plats) {
				if(p.getNom().equals(plat.getNom())) {
					index = i;
				}
				i++;
			}
			this.plats.remove(index);
			trouve = true;
		}
		return trouve;
	}
	
	public List<Plat> getPlats(){
		return this.plats;
	}
	
	public void setPlats(List<Plat> plats) {
		this.plats = plats;
	}
	/**
	 * méthode permettant de mettre à jour la BDD
	 * @throws Exception
	 */
	public void updateBDD() throws Exception {
		Main.connexionBDD();
		System.out.println(this.plats.size());
		String SQL = "DELETE FROM platcarte";
		Statement stmt = Main.connection.createStatement();
		stmt.executeUpdate(SQL);

		for (int i = 0; i < this.plats.size(); i++) {
			SQL = "INSERT INTO platcarte VALUES (?)";

			PreparedStatement pstmt2 = Main.connection.prepareStatement(SQL);
			pstmt2.setInt(1, this.plats.get(i).getId());

			pstmt2.executeUpdate();
		}
	}
}
