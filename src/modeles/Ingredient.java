package modeles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import application.Main;

public class Ingredient {

	private int id;
	private String nom;
	private String unite;

	public Ingredient(String nom, String unite) throws SQLException {
		this.nom = nom;
		this.unite = unite;
	}
	
	public Ingredient(int id) throws SQLException { // pour récupération dans bdd
		this.id = id;
		Ingredient ingredient = getIngredientById(id);
		this.nom = ingredient.getNom();
		this.unite = ingredient.getUnite();
	}

	public Ingredient(int id, String nom, String unite) {
		this.nom = nom;
		this.unite = unite;
		this.id = id;
	}

	/**
	 * méthode permettant de récupérer un ingrédient par son ID
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static Ingredient getIngredientById(int id) throws SQLException {
		String SQL = "SELECT * FROM ingredient WHERE idingredient = ?";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1, id);

		ResultSet rs = pstmt.executeQuery();

		rs.next();

		return new Ingredient(id,rs.getString("nom"), rs.getString("unite"));
	}

	/**
	 * méthode permettant de récupérer l'unité d'un ingrédient par son ID
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static String getUniteIngredientById(int id) throws SQLException {
		String SQL = "SELECT unite FROM ingredient WHERE idingredient = ?";

		PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
		pstmt.setInt(1, id);

		ResultSet rs = pstmt.executeQuery();

		rs.next();

		return rs.getString("unite");
	}

	/**
	 * méthode permettant d'ajouter un ingrédient à la BDD
	 * @param ingredient
	 * @throws SQLException
	 */
	public static void insertIngredientBDD(Ingredient ingredient) throws SQLException {
		int idResult = 0;

		HashMap<Integer, String> liste_ingredient_existant = getListeIngredientExistant();
		if(!liste_ingredient_existant.containsValue(ingredient.getNom())) {
			int id = getMaxId();

			int idStock = Stock.getMaxId();

			String SQL = "INSERT INTO stock VALUES (?, ?)";

			PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
			pstmt.setInt(1, idStock);
			pstmt.setInt(2, 0);

			pstmt.executeUpdate();

			SQL = "INSERT INTO ingredient VALUES (?, ?, ?, ?)";

			PreparedStatement pstmt2 = Main.connection.prepareStatement(SQL);
			pstmt2.setInt(1, id);
			pstmt2.setString(2, ingredient.getNom());
			pstmt2.setString(3, ingredient.getUnite());
			pstmt2.setDouble(4, idStock);

			pstmt2.executeUpdate();
			idResult = id;
				
		}else {
			for (@SuppressWarnings("rawtypes") Map.Entry mapentry : liste_ingredient_existant.entrySet()) {
				if(mapentry.getValue().equals(ingredient.getNom())) {
					idResult = (int) mapentry.getKey();
				}
			}
		}
		ingredient.setId(idResult);
	}


	/**
	 * méthode permettant de récuépérer la liste des ingrédients existants en version HashMap
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Integer, String> getListeIngredientExistant() throws SQLException {
		HashMap<Integer, String> liste_ingredient_existant = new HashMap<Integer, String>();
		String requete = "SELECT idingredient, nom FROM ingredient;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		while (rs.next()) {
			liste_ingredient_existant.put(Integer.parseInt(rs.getString("idingredient")), rs.getString("nom"));
		}
		return liste_ingredient_existant;
	}

	/**
	 * méthode permettant de récupérer la liste des ingrédients en version ArrayList
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Ingredient> listIngredient() throws SQLException {
		ArrayList<Ingredient> liste_ingredient_existant = new ArrayList<Ingredient>();
		String requete = "SELECT * FROM ingredient;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		while (rs.next()) {
			liste_ingredient_existant.add(new Ingredient(Integer.parseInt(rs.getString("idingredient")), rs.getString("nom"), rs.getString("unite")));
		}
		return liste_ingredient_existant;
	}

	/**
	 * méthode permettant d'utiliser un ingrédient, et de mettre à jour la BDD
	 * @param quantite
	 * @throws Exception
	 */
	public void utiliser(double quantite) throws Exception {
		Stock stock = new Stock(this);
		stock.retirer(quantite);
	}


	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public String getUnite() {
		return unite;
	}

	public String toString() {
		return this.id+"";
	}
	
	/**
	 * méthode permettant de récupérer le prochain ID à insérer dans la BDD
	 * @return
	 * @throws SQLException
	 */
	public static int getMaxId() throws SQLException {
		String requete = "SELECT MAX(idingredient) FROM ingredient;";
		Statement stmt = Main.connection.createStatement();
		ResultSet rs = stmt.executeQuery(requete);
		rs.next();
		return rs.getInt(1)+1;	
	}
}
