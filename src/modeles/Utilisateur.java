package modeles;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import application.Main;

public class Utilisateur {

	public static final String SERVEUR = "serveur";
	public static final String ASSISTANT_SERVICE = "assistant";
	public static final String DIRECTEUR = "directeur";
	public static final String CUISINIER = "cuisiner";
	public static final String MAITRE_HOTEL = "maitreHotel";
	public static Scanner console;

	private String nom;
	private String role;

	public Utilisateur(String nom, String role) {
		this.nom = nom;
		this.role = role;
	}

	/**
	 * méthode permettant de trouver un utilisateur
	 * @param nom
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	public static Utilisateur findUtilisateur(String nom, Connection connection) throws SQLException {

		String SQL = "SELECT * FROM utilisateur WHERE nomCompte = ?";

		PreparedStatement pstmt = connection.prepareStatement(SQL);
		pstmt.setString(1, nom);

		ResultSet rs = pstmt.executeQuery();

		rs.next();

		if (rs.getRow() == 1) {
			String role = "";
			switch (rs.getString("role")) {
			case "serveur":
				role = SERVEUR;
				break;
			case "assistant":
				role = ASSISTANT_SERVICE;
				break;
			case "directeur":
				role = DIRECTEUR;
				break;
			case "cuisinier":
				role = CUISINIER;
				break;
			case "maitre":
				role = MAITRE_HOTEL;
				break;
			default:
				role = "non reconnu";
			}
			return new Utilisateur(rs.getString("nomCompte"), role);
		} else {
			return null;
		}
	}

	/**
	 * méthode permettant de créer un utilisateur
	 * @param nomCompte
	 * @throws SQLException
	 */
	public static void creerUtilisateur(String nomCompte) throws SQLException {

		System.out.println("Entrez le rôle du nouvel employé parmi ces rôles : " + SERVEUR + ", " + ASSISTANT_SERVICE
				+ ", " + DIRECTEUR + ", " + CUISINIER + ", " + MAITRE_HOTEL);

		boolean ok = false;

		String choix = " ";
		while (!ok) {
			console = new Scanner(System.in);

			choix = console.nextLine();
			if (!choix.equals(SERVEUR) && !choix.equals(ASSISTANT_SERVICE) && !choix.equals(DIRECTEUR)
					&& !choix.equals(CUISINIER) && !choix.equals(MAITRE_HOTEL)) {
				System.out.println("Ce rôle n'existe pas. Entrez un nouveau rôle");
			} else {
				String sql = "INSERT INTO UTILISATEUR VALUES (?, ?)";
				PreparedStatement stmt = Main.connection.prepareStatement(sql);
				stmt.setString(1, nomCompte);
				stmt.setString(2, choix);
				stmt.executeUpdate();
				ok = true;
			}
		}

	}

	/**
	 * méthode permettant de modifier le role d'un utilisateur
	 * @throws FileNotFoundException
	 * @throws SQLException
	 * @throws IOException
	 */
	public void modifierRole() throws FileNotFoundException, SQLException, IOException {

		System.out.println("Entrez le nouveau rôle de l'employé parmi ces rôles : " + SERVEUR + ", " + ASSISTANT_SERVICE
				+ ", " + DIRECTEUR + ", " + CUISINIER + ", " + MAITRE_HOTEL);

		String choix = " ";
		boolean ok = false;

		while (!ok) {
			console = new Scanner(System.in);

			choix = console.nextLine();
			if (!choix.equals(SERVEUR) && !choix.equals(ASSISTANT_SERVICE) && !choix.equals(DIRECTEUR)
					&& !choix.equals(CUISINIER) && !choix.equals(MAITRE_HOTEL)) {
				System.out.println("Ce rôle n'existe pas. Entrez un nouveau rôle");
			} else {
				ok = true;
				this.role = choix;

				Main.connexionBDD();
				String SQL = "UPDATE utilisateur SET role=? WHERE nomcompte = ?";

				PreparedStatement pstmt = Main.connection.prepareStatement(SQL);
				pstmt.setString(1, this.role);
				pstmt.setString(2, this.nom);

				pstmt.executeUpdate();
			}
		}

	}

	public boolean estServeur() {
		return true;
	}

	public void changerRole(String role) {
		this.role = role;
	}

	public String getRole() {
		return this.role;
	}

	public String getNom() {
		return this.nom;
	}
}
