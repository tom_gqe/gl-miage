package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import modeles.Commande;
import modeles.Plat;
import modeles.Table;
import modeles.Utilisateur;

class TestTable {
    private Table table;

	
    @BeforeEach
    void setup() throws SQLException {
        table = new Table(1, 4, Table.NETTOYEE, new Utilisateur("serveur1", Utilisateur.SERVEUR), new Utilisateur("assistant1", Utilisateur.ASSISTANT_SERVICE));
    }

	@Test
    @DisplayName("table doit être nettoyée par défaut")
	void testInit() {
		assertEquals(Table.NETTOYEE, table.getEtat());
	}
	
    @Test
    @DisplayName("table doit être dressée après dressage")
	void testDresser() {
		table.dresser();
		assertEquals(Table.DRESSEE, table.getEtat());
	}  
    
	@Test
    @DisplayName("table doit être nettoyée pour être dressée")
	void testDresserCondition() throws Exception {
		assertTrue(table.dresser());
		assertFalse(table.dresser());
		table.assignerClient(4);
		assertFalse(table.dresser());
		table.liberer();
		assertFalse(table.dresser());
	}
    
	@Test
    @DisplayName("table doit être occupée après assigner")
	void testAssigner() throws Exception {
		table.dresser();
		table.assignerClient(4);
		assertEquals(Table.OCCUPEE, table.getEtat());
	}
	
	@Test
    @DisplayName("table doit être dressée pour être assignée")
	void testAssignerCondition() throws Exception {
		assertFalse(table.assignerClient(4));
		table.dresser();
		assertTrue(table.assignerClient(4));
		assertFalse(table.assignerClient(4));
		table.liberer();
		assertFalse(table.assignerClient(4));
	}
	
	@Test
    @DisplayName("table doit ne peut être occupée par trop de client")
	void testAssignerNbCouvert() throws Exception {
		table.dresser();
		assertFalse(table.assignerClient(5));
		assertEquals(Table.DRESSEE, table.getEtat());
	}
	
	@Test
    @DisplayName("table doit ne peut être occupée par personne")
	void testAssignerNbCouvertNul() throws Exception {
		table.dresser();
		assertFalse(table.assignerClient(0));
		assertEquals(Table.DRESSEE, table.getEtat());
	}
	
	@Test
    @DisplayName("table doit ne peut être occupée par un nombre négatif")
	void testAssignerNbCouvertNeg() throws Exception {
		table.dresser();
		assertFalse(table.assignerClient(-2));
		assertEquals(Table.DRESSEE, table.getEtat());
	}
	
	@Test
    @DisplayName("table doit ne peut être occupée si déja occupée")
	void testAssignerDejaOccupee() throws Exception {
		table.dresser();
		table.assignerClient(4);
		assertFalse(table.assignerClient(2));
	}
	
	@Test
    @DisplayName("table doit être occupée pour être libérée")
	void testLibererCondition() throws Exception {
		assertFalse(table.liberer());
		table.dresser();
		assertFalse(table.liberer());
		table.assignerClient(4);
		assertTrue(table.liberer());
	}
	
	@Test
    @DisplayName("table doit être sale après etre liberée")
	void testLiberee() throws Exception {
		table.dresser();
		table.assignerClient(4);
		table.liberer();
		assertEquals(Table.SALE, table.getEtat());
	}
	
	@Test
    @DisplayName("table doit être sale pour être nettoyée")
	void testNettoyerCondition() throws Exception {
		assertFalse(table.nettoyer());
		table.dresser();
		assertFalse(table.nettoyer());
		table.assignerClient(4);
		assertFalse(table.nettoyer());
		table.liberer();
		assertTrue(table.nettoyer());
	}
	
	// ce test ne fonctionne que si il n'y a pas de commande en cours sur la table 1
	@Test
    @DisplayName("la facture pour un total de 25€ doit valoir 25")
	void testFacture() throws Exception {
		table.dresser();
		table.assignerClient(4);
		List<Commande> cmd = new ArrayList<Commande>();
		Plat pl1 = new Plat();
		Plat pl2 = new Plat();
		pl1.setId(2);
		pl2.setId(2);
		cmd.add(new Commande(pl1, table, Commande.ATTENTE, true, LocalDateTime.now()));
		cmd.add(new Commande(pl2, table, Commande.ATTENTE, false, LocalDateTime.now()));
		table.validerCommmande(cmd);
		assertEquals(9.0, table.editerFacture());
	}
	

}
