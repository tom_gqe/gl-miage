package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import modeles.Statistique;

class TestStatistique {

	
	Statistique stat;
	
	@BeforeEach
	void setup() {
		stat = new Statistique(LocalDate.now(), 0, 0, 0, 0, 0.0, 0.0);
	}

	@Test
	@DisplayName("statistique sur le temps de préparation moyen")
	void testCommandePreparee() throws Exception {
		stat.commandePreparee(10);
		stat.commandePreparee(20);
		stat.commandePreparee(35);
		stat.commandePreparee(15);
		
		assertEquals(20, stat.getTempsPreparationMoyen());
	}
	
	@Test
	@DisplayName("statistique sur la recette du dejeuner")
	void testRecetteDejeuner() throws Exception {
		stat.clientFacture(20, 80, true);
		stat.clientFacture(10, 30, true);
		stat.clientFacture(15, 50, false);
		stat.clientFacture(30, 150, true);
		
		assertEquals(260, stat.getRecetteDejeuner());
	}
	
	@Test
	@DisplayName("statistique sur la recette du diner")
	void testRecetteDiner() throws Exception {
		stat.clientFacture(20, 80, false);
		stat.clientFacture(10, 30, false);
		stat.clientFacture(15, 50, true);
		stat.clientFacture(30, 150, false);
		
		assertEquals(260, stat.getRecetteDiner());
	}
	
	@Test
	@DisplayName("statistique sur le temps de rotation moyen")
	void testTempsRotationMoyen() throws Exception {
		stat.clientInstalle(2);
		stat.clientFacture(25, 80, false);
		stat.clientInstalle(4);
		stat.clientFacture(10, 30, false);
		stat.clientInstalle(1);
		stat.clientFacture(15, 50, true);
		stat.clientInstalle(1);
		stat.clientFacture(30, 150, false);
		
		assertEquals(10, stat.getTempsRotationMoyen());
	}

}
