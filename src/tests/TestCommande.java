package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import application.Main;
import modeles.Commande;
import modeles.Ingredient;
import modeles.Plat;
import modeles.Table;

class TestCommande {
	

	Plat plat;
	Table table;
	Commande commande;
	
    @BeforeEach
    void setup() throws SQLException, FileNotFoundException, IOException {
    	Main.connexionBDD();
    	Ingredient pate_a_pizza = new Ingredient("Pate_a_pizza", "piece");
    	Ingredient.insertIngredientBDD(pate_a_pizza);
    	Ingredient sauce_tomate = new Ingredient("sauce_tomate", "kg");
    	Ingredient.insertIngredientBDD(sauce_tomate);
    	Ingredient mozzarella = new Ingredient("mozzarella", "kg");
    	Ingredient.insertIngredientBDD(mozzarella);
    	ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>(Arrays.asList(pate_a_pizza, sauce_tomate, mozzarella));
    	ArrayList<Double> quantites = new ArrayList<Double>(Arrays.asList(1.0, 0.1, 0.1));
		plat = new Plat("Pizza", "StreetFood", 9.00, ingredients, quantites);
		Plat.insertPlatBDD(plat);
		table = new Table(1, 4);
		
		commande = new Commande(plat, table, Commande.ATTENTE, false, LocalDateTime.of(2021, 5, 3, 0, 0));
    }
    

	@Test
    @DisplayName("preparation d'une commande | passage à l'état preparee")
	void testPreparer() throws Exception {
		assertEquals(Commande.ATTENTE, commande.getEtat());
		commande.preparer();
		assertEquals(Commande.PREPAREE, commande.getEtat());
	}
	
	@Test
    @DisplayName("payer une commande | passage à l'état facturee")
	void testPayer() throws Exception {
		assertEquals(Commande.ATTENTE, commande.getEtat());
		commande.preparer();
		assertEquals(Commande.PREPAREE, commande.getEtat());
		commande.payer();
		assertEquals(Commande.FACTUREE, commande.getEtat());
	}
	
	@Test
    @DisplayName("payer une commande sans l'avoir preparée | non passage à l'état facturee")
	void testPayerSansAvoirPrepare() throws Exception {
		assertEquals(Commande.ATTENTE, commande.getEtat());
		commande.payer();
		assertEquals(Commande.ATTENTE, commande.getEtat());
	}
	
	@Test
    @DisplayName("servir un plat d'une commande | passage à l'état servie")
	void testServirPlat() throws Exception {
		commande.preparer();
		assertEquals(Commande.PREPAREE, commande.getEtat());
		commande.servir();
		assertEquals(Commande.SERVIE, commande.getEtat());
	}

}
