package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import application.Main;
import modeles.Ingredient;
import modeles.Stock;

class TestStock {

	Stock stock;

	@BeforeEach
	void setup() throws Exception {
		Main.connexionBDD();
		Ingredient ingredient = new Ingredient("Pate_a_pizza", "piece");
		Ingredient.insertIngredientBDD(ingredient);
		stock = new Stock(ingredient);
		stock.setQuantite(0);
	}

	@Test
	@DisplayName("ajout d'une quantité d'un ingredient")
	void testAjouteQuantite() throws Exception {
		assertEquals(0.0, stock.getQuantite());
		stock.ajouter(10.0);
		assertEquals(10.0, stock.getQuantite());
	}

	@Test
	@DisplayName("ajout d'une quantité négative d'un ingredient")
	void testAjouteQuantiteNegative() throws Exception {
		assertEquals(0.0, stock.getQuantite());
		stock.ajouter(-10.0);
		assertEquals(0.0, stock.getQuantite());
	}

	@Test
	@DisplayName("retire une quantité d'un ingredient")
	void testRetireQuantite() throws Exception {
		assertEquals(0.0, stock.getQuantite());
		stock.ajouter(10.0);
		assertEquals(10.0, stock.getQuantite());
		stock.retirer(2.0);
		assertEquals(8.0, stock.getQuantite());
	}

	@Test
	@DisplayName("retire une quantité positive d'un ingredient")
	void testRetireQuantitePositive() throws Exception {
		assertEquals(0.0, stock.getQuantite());
		stock.ajouter(10.0);
		assertEquals(10.0, stock.getQuantite());
		stock.retirer(-2.0);
		assertEquals(10.0, stock.getQuantite());
	}

	@Test
	@DisplayName("retire une quantité qui mène à inférieur à 0 d'un ingredient")
	void testRetireQuantiteInf0() throws Exception {
		assertEquals(0.0, stock.getQuantite());
		stock.ajouter(10.0);
		assertEquals(10.0, stock.getQuantite());
		stock.retirer(20.0);
		assertEquals(0.0, stock.getQuantite());  // soit on prend en compte et on bloque à zéro, soit on prend pas en compte ?
	}


}
