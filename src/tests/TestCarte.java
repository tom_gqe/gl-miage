package tests;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import application.Main;
import modeles.Carte;
import modeles.Ingredient;
import modeles.Plat;

class TestCarte {

	Carte carte;
	Ingredient pate_a_pizza;
	Ingredient sauce_tomate;
	Ingredient mozzarella;
	ArrayList<Ingredient> ingredients;
	ArrayList<Double> quantites;
	Plat plat;
	
    @BeforeEach
    void setup() throws Exception {
    	Main.connexionBDD();
    	carte = new Carte();
    	carte.setPlats(new ArrayList<Plat>());
    	pate_a_pizza = new Ingredient("Pate_a_pizza", "piece");
    	sauce_tomate = new Ingredient("sauce_tomate", "kg");
    	mozzarella = new Ingredient("mozzarella", "kg");
    	ingredients = new ArrayList<Ingredient>(Arrays.asList(pate_a_pizza, sauce_tomate, mozzarella));
    	quantites = new ArrayList<Double>(Arrays.asList(1.0, 0.1, 0.1));
		plat = new Plat("Pizza", "StreetFood", 9.00, ingredients, quantites);
		assertTrue(carte.ajouterPlat(plat));
		assertEquals(1, carte.getPlats().size());
    }

	
	@Test
    @DisplayName("ajouter un plat déjà existant à la carte du jour")
	void testAjoutePlatExistant() throws Exception {
		assertEquals(1, carte.getPlats().size());
		assertFalse(carte.ajouterPlat(plat));
		assertEquals(1, carte.getPlats().size());
	}
	
	@Test
    @DisplayName("retirer un plat existant à la carte du jour")
	void testRetirerPlat() throws Exception {
		assertEquals(1, carte.getPlats().size());
		assertTrue(carte.retirerPlat(plat));
		assertEquals(0, carte.getPlats().size());
	}
	
	@Test
    @DisplayName("retirer un plat inexistant à la carte du jour")
	void testRetirerPlatInexistant() throws Exception {
    	Ingredient pain = new Ingredient("pain", "piece");
    	ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>(Arrays.asList(pain));
    	ArrayList<Double> quantites = new ArrayList<Double>(Arrays.asList(10.0));
		Plat plat = new Plat("Burger", "StreetFood", 9.00, ingredients, quantites);
		
		int index = -1;
		for(int i = 0; i < carte.getPlats().size(); i++) {
			if( carte.getPlats().get(i).getNom().equals("Burger")) {
				index = i;
			}
		}
		assertEquals(-1, index);
		assertFalse(carte.retirerPlat(plat));
		assertEquals(-1, index);
	}
	

}
