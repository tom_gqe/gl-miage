package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import application.Main;
import modeles.Ingredient;
import modeles.Plat;
import modeles.Stock;

class TestPlat {
	
	private Plat plat;
	private Ingredient mozzarella;
	private Ingredient pate_a_pizza;
	private Ingredient sauce_tomate;

    @BeforeEach
    void setup() throws SQLException, FileNotFoundException, IOException {
    	Main.connexionBDD();
    	pate_a_pizza = new Ingredient("Pate_a_pizza", "piece");
    	Ingredient.insertIngredientBDD(pate_a_pizza);
    	sauce_tomate = new Ingredient("sauce_tomate", "kg");
    	Ingredient.insertIngredientBDD(sauce_tomate);
    	mozzarella = new Ingredient("mozzarella", "kg");
    	Ingredient.insertIngredientBDD(mozzarella);
    	ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>(Arrays.asList(pate_a_pizza, sauce_tomate, mozzarella));
    	ArrayList<Double> quantites = new ArrayList<Double>(Arrays.asList(1.0, 0.1, 0.1));
		plat = new Plat("Pizza", "StreetFood", 9.00, ingredients, quantites);
		Plat.insertPlatBDD(plat);
    }
    
	@Test
    @DisplayName("ajout d'un nouvel ingredient avec la méthode ajouterIngredient")
	void testAjouteIngredient() throws Exception {
		assertEquals(3, plat.getIngredients().size());
		Ingredient champignon = new Ingredient("champignon", "kg");
		Ingredient.insertIngredientBDD(champignon);
		plat.ajouterIngredient(champignon, 0.1);
		assertEquals(4, plat.getIngredients().size());
	}

	
	@Test
    @DisplayName("ajout d'un ingredient existant avec la méthode ajouterIngredient")
	void testAjouteIngredientExistant() throws Exception {

		double qte = plat.getQuantites().get(2);
		assertEquals(qte, plat.getQuantites().get(2));
		int size = plat.getIngredients().size();
		mozzarella = new Ingredient("mozzarella", "kg");
		plat.ajouterIngredient(mozzarella, 0.2);
		// l'ingrédient ne s'ajoute pas
		assertEquals(size, plat.getIngredients().size());
		// modifie la quantité de l'ingrédient
		assertEquals(0.2, plat.getQuantites().get(2));
	}
	
	@Test
    @DisplayName("modification d'une quantite d'ingredient avec la méthode ajouterIngredient")
	void testModifierQuantiteIngredient() throws Exception {
		assertEquals(0.1, plat.getQuantites().get(2));
		plat.ajouterIngredient(mozzarella, 0.2);
		assertEquals(0.2, plat.getQuantites().get(2));
	}
	
	
	@Test
    @DisplayName("mise en route de la cuisson du plat et mise à jour des stock des ingrédients")
	void testCuisiner() throws Exception {
		Stock stock_mozzarella = new Stock(mozzarella);
		stock_mozzarella.setQuantite(10.0);
		Stock stock_pate_a_pizza = new Stock(pate_a_pizza);
		stock_pate_a_pizza.setQuantite(10.0);
		Stock stock_sauce_tomate = new Stock(sauce_tomate);
		stock_sauce_tomate.setQuantite(10.0);
		
		assertEquals(10.0, stock_mozzarella.getQuantiteBDD());
		assertEquals(10.0, stock_pate_a_pizza.getQuantiteBDD());
		assertEquals(10.0, stock_sauce_tomate.getQuantiteBDD());
		plat.cuisiner();
		assertEquals(9.9, stock_mozzarella.getQuantiteBDD());
		assertEquals(9.0, stock_pate_a_pizza.getQuantiteBDD());
		assertEquals(9.9, stock_sauce_tomate.getQuantiteBDD());
	}
	
	@Test
    @DisplayName("retrait d'un ingredient existant avec la méthode retirerIngredient")
	void testRetirerIngredientExistant() throws Exception {
		assertEquals(mozzarella, plat.getIngredients().get(2));
		plat.retirerIngredient(mozzarella);
		
		int index = -1;
		for(int i = 0; i < plat.getIngredients().size(); i++) {
			if( plat.getIngredients().get(i).getNom().equals(mozzarella.getNom())) {
				index = i;
			}
		}
		assertEquals(-1, index);
	}
	
	@Test
    @DisplayName("retrait d'un ingredient inexistant avec la méthode retirerIngredient")
	void testRetirerIngredientInexistant() throws Exception {
		Ingredient jambon = new Ingredient("jambon", "kg");
		boolean trouve = false;
		for(Ingredient ingre : plat.getIngredients()) {
			if(ingre == jambon) {
				trouve = true;
			}
		}
		assertFalse(trouve);
		assertEquals(3, plat.getIngredients().size());
		plat.retirerIngredient(jambon);
		assertEquals(3, plat.getIngredients().size());
	}

}
