package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Scanner;

import modeles.Carte;
import modeles.Commande;
import modeles.Ingredient;
import modeles.Plat;
import modeles.Statistique;
import modeles.Table;
import modeles.Utilisateur;

public class Main {

	public static Utilisateur UTILISATEUR_COURANT;

	public static Connection connection;

	public static Scanner console;

	// initialise la connexion à la BDD puis créer une instance de main qui servira
	// de session courante
	public static void main(String[] args) throws Exception {
		boolean bddOK = true;
		try {
			connexionBDD();
		} catch (SQLException e) {
			bddOK = false;
			System.err.println("La connexion à la base de données a échoué");
			System.err.println(e.getMessage());
		} catch (FileNotFoundException e) {
			bddOK = false;
			System.err.println("Le fichier config.properties est introuvable");
			System.err.println(e.getMessage());
		} catch (IOException e) {
			bddOK = false;
			System.err.println("Le fichier config.properties est incomplet");
			System.err.println(e.getMessage());
		}
		if (bddOK) {
			System.out.println("Démarrage de l'application");
			new Main();
		}

	}

	// A la creation d'une session, on affiche l'écran d'accueil
	public Main() throws Exception {
		try {
			System.out.println(Statistique.findStatistique(LocalDate.now()));
			ecranAccueil();
		} catch (SQLException e) {
			System.err.println("La connexion à la base de donnée a échoué");
			e.printStackTrace();
		}
	}

	// liaison entre la BDD et l'application
	// throws exception si echec
	public static void connexionBDD() throws SQLException, FileNotFoundException, IOException {
		File file = new File("config.properties");
		Properties properties = new Properties();
		properties.load(new FileInputStream(file));
		String url = properties.getProperty("url");
		String user = properties.getProperty("user");
		String password = properties.getProperty("password");
		connection = DriverManager.getConnection(url, user, password);
	}

	// premier écran affiché
	private void ecranAccueil() throws Exception {
		UTILISATEUR_COURANT = null;
		System.out.println("Que souhaitez vous faire ?");
		System.out.println("1 - Se connecter");
		System.out.println("0 - Quitter");
		int choix = getInt(1);
		switch (choix) {
		case 0:
			System.exit(0);
			break;
		case 1:
			ecranConnexion();
			break;
		default:
		}
	}

	// zone où l'utilisateur renseigne son nom
	private void ecranConnexion() throws Exception {
		System.out.println("Entrez votre nom d'utilisateur");
		String entreeUtilisateur = getEntreeNomCompte();
		if (connexionUtilisateur(entreeUtilisateur)) {
			ecranAccueilConnecte();
		} else {
			System.out.println("Votre nom ne correspond à aucun compte");
			ecranAccueil();
		}

	}

	private boolean connexionUtilisateur(String nomCompte) throws SQLException {
		UTILISATEUR_COURANT = Utilisateur.findUtilisateur(nomCompte, connection);
		return (UTILISATEUR_COURANT != null);
	}

	private Utilisateur estUtilisateur(String nomCompte) throws SQLException {
		return (Utilisateur.findUtilisateur(nomCompte, connection));
	}

	// récupère les 25 premiers caractères de la saisie utilisateur et boucle tant
	// que son choix declenche une exception
	private String getEntreeNomCompte() {
		String choix = " ";
		while (choix == " ") {
			console = new Scanner(System.in);
			try {
				choix = console.nextLine();
				if (choix.length() > 25) {
					choix = choix.substring(0, 24);
				}
			} catch (Exception e) {
				System.err.println(e.getMessage());
				System.out.println("Cette commande n'est pas recevable");
				System.out.println("Entrez votre nouveau choix");
			}
		}
		return choix;
	}

	// récupère les n premiers caractères de la saisie utilisateur et boucle tant
	// que son choix declenche une exception
	private String getString(int n) {
		String choix = " ";
		while (choix == " ") {
			console = new Scanner(System.in);
			try {
				choix = console.nextLine();
				if (choix.length() > n) {
					choix = choix.substring(0, n - 1);
				}
			} catch (Exception e) {
				System.err.println(e.getMessage());
				System.out.println("Cette commande n'est pas recevable");
				System.out.println("Entrez votre nouveau choix");
			}
		}
		return choix;
	}

	// récupère la saisie utilisateur et boucle tant que son choix ne fait pas parti
	// de la liste
	private double getDouble() {
		double choix = -1;
		boolean ok = false;
		while (!ok) {
			console = new Scanner(System.in);
			try {
				choix = console.nextDouble();
				if (!(choix < 0 || choix > Double.MAX_VALUE)) {
					ok = true;
				}
			} catch (InputMismatchException e) {

			}
			if (!ok) {
				System.out.println("Cette commande n'est pas recevable");
				System.out.println("Entrez votre nouveau choix");
			}
		}
		return choix;
	}

	// Fait choisir un double entre min et max à l'utilisateur
	private double getDouble(double min, double max) {
		double choix = min;
		boolean ok = false;
		while (!ok) {
			console = new Scanner(System.in);
			try {
				choix = console.nextDouble();
				if (!(choix < min || choix > max)) {
					ok = true;
				}
			} catch (InputMismatchException e) {

			}
			if (!ok) {
				System.out.println("Cette commande n'est pas recevable");
				System.out.println("Entrez votre nouveau choix");
			}
		}
		return choix;
	}

	// récupère la saisie utilisateur et boucle tant que son choix ne fait pas parti
	// de la liste
	private int getInt(int intMax) {
		int choix = -1;
		boolean ok = false;
		while (!ok) {
			console = new Scanner(System.in);
			try {
				choix = console.nextInt();
				if (!(choix < 0 || choix > intMax)) {
					ok = true;
				}
			} catch (InputMismatchException e) {

			}
			if (!ok) {
				System.out.println("Cette commande n'est pas recevable");
				System.out.println("Entrez votre nouveau choix");
			}
		}
		return choix;
	}

	// Fait choisir un entier entre min et max à l'utilisateur
	private int getInt(int min, int intMax) {
		int choix = min;
		boolean ok = false;
		while (!ok) {
			console = new Scanner(System.in);
			try {
				choix = console.nextInt();
				if (!(choix < min || choix > intMax)) {
					ok = true;
				}
			} catch (InputMismatchException e) {

			}
			if (!ok) {
				System.out.println("Cette commande n'est pas recevable");
				System.out.println("Entrez votre nouveau choix");
			}
		}
		return choix;
	}

	// Affiche les options de l'utilisateur connecté
	// On appelle la fonction qui correspond au role et au choix de l'utilisateur
	private void ecranAccueilConnecte() throws Exception {
		System.out.println("Que souhaitez vous faire ?");
		String choix = UTILISATEUR_COURANT.getRole() + 0;
		System.out.println("0 - Se Déconnecter");

		int nbAServir = getListeCommandeAServir().size();

		switch (UTILISATEUR_COURANT.getRole()) {
		case Utilisateur.ASSISTANT_SERVICE:
			System.out.println("1 - Voir mes tables");
			choix = Utilisateur.ASSISTANT_SERVICE + getInt(1);
			break;
		case Utilisateur.CUISINIER:
			System.out.println("1 - Consulter les stocks");
			System.out.println("2 - Definir un plat");
			System.out.println("3 - Voir les commandes en attente");
			choix = Utilisateur.CUISINIER + getInt(3);
			break;
		case Utilisateur.DIRECTEUR:
			System.out.println("1 - Consulter les stocks");
			System.out.println("2 - Definir un plat");
			System.out.println("3 - Voir les tables de l'étage");
			System.out.println("4 - Gérer les employés");
			System.out.println("5 - Voir les statistiques du jour");
			System.out.println("6 - Modifier la carte");
			choix = Utilisateur.DIRECTEUR + getInt(6);
			break;
		case Utilisateur.MAITRE_HOTEL:
			System.out.println("1 - Voir les tables de l'étage");
			System.out.println("2 - Facturer une table");
			choix = Utilisateur.MAITRE_HOTEL + getInt(2);
			break;
		case Utilisateur.SERVEUR:
			System.out.println("1 - Voir mes tables");
			System.out.println("2 - Servir les plats prêts (" + nbAServir + ")");
			System.out.println("3 - Voir les 5 plats les plus populaires");
			choix = Utilisateur.SERVEUR + getInt(3);
			break;
		default:
			System.err.println("Votre role n'est pas pris en charge");
			choix = UTILISATEUR_COURANT.getRole() + 0;
			break;
		}

		switch (choix) {
		case Utilisateur.ASSISTANT_SERVICE + 1:
			listerTables();
		break;
		case Utilisateur.CUISINIER + 1:
			consulterLesStocks();
		break;
		case Utilisateur.CUISINIER + 2:
			definirPlat();
		break;
		case Utilisateur.CUISINIER + 3:
			voirFileAttente();
		break;
		case Utilisateur.DIRECTEUR + 1:
			consulterLesStocks();
		break;
		case Utilisateur.DIRECTEUR + 2:
			definirPlat();
		break;
		case Utilisateur.DIRECTEUR + 3:
			listerTables();
		break;
		case Utilisateur.DIRECTEUR + 4:
			gererEmployes();
		case Utilisateur.DIRECTEUR + 5:
			listerStatistiques();
		ecranAccueilConnecte();
		break;
		case Utilisateur.DIRECTEUR + 6:
			modifierCarte();
		break;
		case Utilisateur.MAITRE_HOTEL + 1:
			listerTables();
		break;
		case Utilisateur.MAITRE_HOTEL + 2:
			facturerTable();
		break;
		case Utilisateur.SERVEUR + 1:
			listerTables();
		ecranAccueilConnecte();
		break;
		case Utilisateur.SERVEUR + 2:
			servirTable();
		ecranAccueilConnecte();
		break;
		case Utilisateur.SERVEUR + 3:
			afficherLes5PlatsPopulaire();
		ecranAccueilConnecte();
		break;
		default:
			ecranAccueil();
		}

	}

	// gère l'ajout et la modifictaion d'un employé
	private void gererEmployes() throws Exception {
		System.out.println("Que souhaitez vous faire ?");
		System.out.println("1 - Créer un nouvel employé");
		System.out.println("2 - Modifier le statut d'un employé");
		int choix = getInt(2);

		Utilisateur utilisateur = null;
		boolean ok = false;
		switch (choix) {
		case 1:
			System.out.println("Entrez le nom de compte pour le nouvel utilisateur");
			while (!ok) {
				String nomCompte = getEntreeNomCompte();
				utilisateur = estUtilisateur(nomCompte);

				if (utilisateur == null) {
					Utilisateur.creerUtilisateur(nomCompte);
					ok = true;
				} else {
					System.out.println("Un utilisateur possède déjà ce nom de compte");
					System.out.println("Entrez un nouveau nom de compte");
				}
			}
			break;
		case 2:
			System.out.println("Voici la liste des employés :");

			String requete = "SELECT nomCompte FROM utilisateur";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(requete);
			System.out.println("---------------------------------------");
			while (rs.next()) {
				System.out.println(rs.getString("nomCompte"));
			}
			System.out.println("---------------------------------------");
			System.out.println("Entrez le nom d'un utilisateur dont vous souhaitez modifier le rôle");

			while (!ok) {
				String nomCompte = getEntreeNomCompte();
				utilisateur = estUtilisateur(nomCompte);
				if (utilisateur != null) {
					utilisateur.modifierRole();
					System.out.println("Le rôle de l'utilisateur " + nomCompte + " a été modifié");
					ok = true;
				} else {
					System.out.println("Aucune utilisateur ne possède ce nom de compteur");
					System.out.println("Entrez un nouveau nom d'utilisateur");
				}
			}
			break;
		default:
			ecranAccueil();
		}

	}
	// facture la table choisie
	private void facturerTable() throws Exception {
		List<Table> listeTable = Table.listTableOccupee();
		double addition;

		int i = 1;
		if (listeTable.size() > 0) {
			System.out.println("Entrez le numéro d'une table pour accéder à sa facture");
			for (Table table : listeTable) {
				System.out.println(i++ + ": Table n°" + table.getNumero() + " " + table.getEtat());
			}
			System.out.println("0 - Retour");
			int choix = getInt(listeTable.size() + 1);
			if (choix == 0) {
				ecranAccueilConnecte();
			} else {
				Table tableChoisie = listeTable.get(choix - 1);
				if (tableChoisie == null) {
					System.out.println("Aucune table ne correspond à ce numéro");
				} else {
					addition = tableChoisie.editerFacture();
					System.out.println("La facture de la table " + tableChoisie.getNumero()
					+ " a été editée. Elle s'élève à " + addition + " euros");
					tableChoisie.payerCommande();
					Statistique.clientFactureNow(
							(int) Duration.between(Commande.getDateCommandeBytable(tableChoisie.getNumero()),
									LocalDateTime.now()).toSeconds(),
							addition, LocalTime.now().isAfter(LocalTime.of(18, 00)));
				}
			}
		} else {
			System.out.println("Aucune table n'a était trouvé");
		}
	}

	// permet de lister les statistiques
	private void listerStatistiques() throws SQLException {
		Statistique stat = Statistique.findStatistique(LocalDate.now());
		System.out.println("Temps de preparation moyen : " + stat.getTempsPreparationMoyen() + " secondes");
		System.out.println("Temps de rotation moyen " + stat.getTempsRotationMoyen() + " secondes");
		System.out.println("Recette du déjeuner : " + stat.getRecetteDejeuner() + " €");
		System.out.println("Recette du déjeuner : " + stat.getRecetteDiner() + " €");
	}

	/**
	 * permet d'afficher les 5 plats les plus populaires
	 * @throws SQLException
	 */
	private void afficherLes5PlatsPopulaire() throws SQLException {
		List<String> liste = Statistique.get5PlatPopulaire();
		System.out.println("---------------------------------------");
		for (String nomPlat : liste) {
			System.out.println(nomPlat);
		}
		System.out.println("---------------------------------------");
	}

	/**
	 * permet de servir les plats d'une table 
	 * @throws Exception
	 */
	private void servirTable() throws Exception {
		ArrayList<Commande> liste_commande = getListeCommandeAServir();
		ArrayList<Integer> liste_idCommande = new ArrayList<Integer>();
		int i = 1;
		for (Commande commande : liste_commande) {
			liste_idCommande.add(commande.getId());
			System.out.println(i + " - " + commande.getPlat().getNom());
			i++;
		}
		System.out.println("0 - Retour");
		int choix = getInt(liste_commande.size());
		if (choix == 0) {
			ecranAccueilConnecte();
		} else {
			Commande c = liste_commande.get(choix - 1);
			c.servir();
			System.out.println("| Plat servi avec succès |");
		}
	}

	/**
	 * permet de recupérer la liste des commandes à servir
	 * @return liste des commandes à servir
	 * @throws SQLException
	 */
	private ArrayList<Commande> getListeCommandeAServir() throws SQLException {
		ArrayList<Commande> liste_commande = new ArrayList<Commande>();
		String requete = "SELECT commande.idcommande, plat.nom FROM commande INNER JOIN plat on commande.idplat = plat.idplat INNER JOIN tablec ON commande.numerotable = tablec.numero WHERE commande.etatavancement = 'preparee' AND tablec.nomserveur = ?;";
		PreparedStatement pstmt = Main.connection.prepareStatement(requete);
		pstmt.setString(1, UTILISATEUR_COURANT.getNom());
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			liste_commande.add(new Commande(rs.getInt("idcommande")));
		}
		return liste_commande;
	}

	/**
	 * permet de voir la file d'attente des commandes
	 * @throws Exception
	 */
	private void voirFileAttente() throws Exception {
		ArrayList<Commande> file = Commande.listCommandeAttente();
		if (file.size() > 0) {
			for (Commande commande : file) {
				System.out.println("Commande n°" + commande.getId() + " : " + commande.getPlat().getNom());
			}
			System.out.println("1 - Cuisiner " + file.get(0).getPlat().getNom());
			System.out.println("0 - Retour");
			int choix = getInt(1);
			switch (choix) {
			case 1:
				file.get(0).preparer();
				break;
			default:
				ecranAccueilConnecte();
				break;
			}
		} else {
			System.out.println("Il n'y a pas de commande en attente.");
			ecranAccueilConnecte();
		}
	}

	/**
	 * permet de consulter les stocks des ingrédients
	 * @throws Exception
	 */
	private void consulterLesStocks() throws Exception {
		String requete = "SELECT ingredient.nom, stock.quantite FROM ingredient inner join stock ON ingredient.idingredient = stock.idstock;";
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(requete);
			System.out.println("---------------------------------------");
			while (rs.next()) {
				System.out.println(rs.getString("nom") + " : " + rs.getString("quantite"));
			}
			System.out.println("---------------------------------------");
			ecranAccueilConnecte();
		} catch (SQLException e) {
			System.err.println("Erreur dans la consultation des stocks");
			e.printStackTrace();
		}
	}

	/**
	 * permet de definir un nouveau plat
	 * @throws Exception
	 */
	private void definirPlat() throws Exception {
		ArrayList<String> liste_plat_existant = Plat.getListePlatExistant();
		// choix du nom
		console = new Scanner(System.in).useLocale(Locale.US);
		System.out.println("Nom du plat ?");
		String nomPlat = console.nextLine();
		while (liste_plat_existant.contains(nomPlat)) {
			System.out.println("Plat déjà existant, veuillez saisir un nom différent.");
			nomPlat = console.nextLine();
		}
		// choix categorie
		System.out.println("Catégorie ?");
		String categorie = getString(50);

		// choix prix
		System.out.println("Prix (double) ?");
		Double prix = getDouble();

		// choix ingredients
		ArrayList<Ingredient> liste_ingredients = new ArrayList<Ingredient>();
		ArrayList<Double> liste_quantites = new ArrayList<Double>();
		ArrayList<Ingredient> liste_ingredient_existant = Ingredient.listIngredient();
		System.out.println("Combien d'ingrédients voulez-vous ajouter ? (max " + liste_ingredient_existant.size() + ")");
		int nbIngredients = getInt(1, liste_ingredient_existant.size());
		for (int i = 0; i < nbIngredients; i++) {
			int num = 0;
			System.out.println("Voici la liste des ingrédients : ");
			for (Ingredient ingredient : liste_ingredient_existant) {
				System.out.println("		" + num++ + " - " + ingredient.getNom());
			}
			int size = liste_ingredient_existant.size() + 1;
			System.out.println("		" + size + " - Annuler");
			int choix = getInt(size);
			if (choix == size) {
				ecranAccueilConnecte();
			} else {
				liste_ingredients.add(liste_ingredient_existant.get(choix));
				// choix quantite
				System.out.println("Veuillez choisir une quantité pour l'ingrédient choisi : (en "
						+ liste_ingredient_existant.get(choix).getUnite() + ")");
				double quantite = getDouble(0, Double.MAX_VALUE);
				liste_quantites.add(quantite);
			}
		}
		Plat nouveauPlat = new Plat(nomPlat, categorie, prix, liste_ingredients, liste_quantites);
		Plat.insertPlatBDD(nouveauPlat);
		System.out.println("Ajout du plat terminé");
		System.out.println("---------------------------------------");
		ecranAccueilConnecte();
	}

	/**
	 * permet de lister les tables
	 * @throws Exception
	 */
	private void listerTables() throws Exception {
		List<Table> listeTable = Table.listTable(UTILISATEUR_COURANT);
		int i = 1;
		if (listeTable.size() > 0) {
			System.out.println("Entrez le numéro d'une table pour accéder à ses options");
			for (Table table : listeTable) {
				System.out.println(i++ + ": Table n°" + table.getNumero() + " " + table.getEtat());
			}
			System.out.println("0 - Retour");
			int choix = getInt(listeTable.size() + 1);
			if (choix == 0) {
				ecranAccueilConnecte();
			} else {
				Table tableChoisie = listeTable.get(choix - 1);
				if (tableChoisie == null) {
					System.out.println("Aucune table ne correspond à ce numéro");
				} else {
					optionsTable(tableChoisie);
				}
			}
		} else {
			System.out.println("Aucune table n'a était trouvé");
		}

	}

	/**
	 * permet d'afficher les options sur les tables
	 * @param tableChoisie
	 * @throws Exception
	 */
	private void optionsTable(Table tableChoisie) throws Exception {
		String choix = UTILISATEUR_COURANT.getRole() + 0;
		System.out.println("---------------------------------------");
		System.out.println(tableChoisie);
		System.out.println("---------------------------------------");
		System.out.println("0 - Retour");
		switch (UTILISATEUR_COURANT.getRole()) {
		case Utilisateur.SERVEUR:
			if (tableChoisie.getEtat().equals(Table.DRESSEE)) {
				System.out.println("1 - Assigner clients à la table");
				choix = UTILISATEUR_COURANT.getRole() + getInt(1);
			} else if (tableChoisie.getEtat().equals(Table.OCCUPEE)) {
				System.out.println("1 - Prendre la commande");
				choix = UTILISATEUR_COURANT.getRole() + getInt(1);
			} else {
				choix = UTILISATEUR_COURANT.getRole() + getInt(0);
			}
			break;
		case Utilisateur.MAITRE_HOTEL:
			System.out.println("1 - Assigner clients à la table");
			System.out.println("2 - Assigner un nouveau serveur à la table");
			System.out.println("3 - Assigne un nouvel assitant de service à la table");
			choix = UTILISATEUR_COURANT.getRole() + getInt(3);
			break;
		case Utilisateur.ASSISTANT_SERVICE:
			System.out.println("1 - Modifier l'état de la table");
			choix = UTILISATEUR_COURANT.getRole() + getInt(1);
			break;
		default:
		}

		switch (choix) {
		case Utilisateur.SERVEUR + 1:
			if (tableChoisie.getEtat().equals(Table.DRESSEE)) {
				attribuerClient(tableChoisie);
			} else if (tableChoisie.getEtat().equals(Table.OCCUPEE)) {
				prendreCommande(tableChoisie, new ArrayList<Commande>());
			} else {
				ecranAccueilConnecte();
			}
		break;
		case Utilisateur.SERVEUR + 2:
			break;
		case Utilisateur.ASSISTANT_SERVICE + 1:
			modifierEtatTable(tableChoisie);
		break;
		case Utilisateur.MAITRE_HOTEL + 1:
			attribuerClient(tableChoisie);
		break;
		case Utilisateur.MAITRE_HOTEL + 2:
			attribuerServeur(tableChoisie);
		break;
		case Utilisateur.MAITRE_HOTEL + 3:
			attribuerAssistant(tableChoisie);
		break;
		default:
			ecranAccueilConnecte();
		}

	}

	/**
	 * permet d'attribuer des clients à une table
	 * @param tableChoisie
	 * @throws Exception
	 */
	private void attribuerClient(Table tableChoisie) throws Exception {
		System.out.println("Nombre de client à placer ? (entre 1 et " + tableChoisie.getNbCouverts() + ")");
		int nbClient = getInt(1, tableChoisie.getNbCouverts());
		tableChoisie.assignerClient(nbClient);
		System.out.println("Action effectuée");
		ecranAccueilConnecte();
	}

	/**
	 * permet d'attribuer un serveur à une table
	 * @param tableChoisie
	 * @throws Exception
	 */
	private void attribuerServeur(Table tableChoisie) throws Exception {
		Utilisateur utilisateur = getUtilisateur(Utilisateur.SERVEUR);
		tableChoisie.assignerServeur(utilisateur);
		System.out.println("Action effectuée");
		ecranAccueilConnecte();
	}

	/**
	 * permet d'attribuer un assistant à une table
	 * @param tableChoisie
	 * @throws Exception
	 */
	private void attribuerAssistant(Table tableChoisie) throws Exception {
		Utilisateur utilisateur = getUtilisateur(Utilisateur.ASSISTANT_SERVICE);
		tableChoisie.assignerAssistant(utilisateur);
		System.out.println("Action effectuée");
		ecranAccueilConnecte();
	}

	/**
	 * permet de modifier l'état d'une table
	 * @param tableChoisie
	 * @throws Exception
	 */
	public void modifierEtatTable(Table tableChoisie) throws Exception {

		switch (tableChoisie.getEtat()) {
		case Table.OCCUPEE:
			System.out.println("Cette table est occupée vous ne pouvez pas agir dessus pour le moment");
			ecranAccueilConnecte();
			break;
		case Table.DRESSEE:
			System.out.println("La table est déjà dressée, vous n'avez plus rien à faire");
			ecranAccueilConnecte();
			break;
		case Table.NETTOYEE:
			System.out.println("La table est nettoyée.  Souhaitez-vous dresser la table ?");
			break;
		case Table.SALE:
			System.out.println("La table est sale. Souhaitez-vous la nettoyer ?");
			break;
		}
		System.out.println("1 - Oui");
		System.out.println("2 - Non");
		int choix = getInt(2);

		if (choix == 1) {
			tableChoisie.changerEtatTable();
		} else {
			ecranAccueilConnecte();
		}
		ecranAccueilConnecte();
	}

	/**
	 * permet de récupérér un utilisateur
	 * @param utilisateurVoulu
	 * @return
	 * @throws SQLException
	 */
	private Utilisateur getUtilisateur(String utilisateurVoulu) throws SQLException {
		Utilisateur utilisateur = null;
		boolean ok = false;

		System.out.println("Entrez le nom d'un " + utilisateurVoulu);
		while (!ok) {
			String entreeUtilisateur = getEntreeNomCompte();
			utilisateur = estUtilisateur(entreeUtilisateur);

			if (!(utilisateur == null) && utilisateur.getRole().equals(utilisateurVoulu)) {
				ok = true;
			} else if (!(utilisateur == null) && !(utilisateur.getRole().equals(utilisateurVoulu))) {
				System.out.println("L'utilisateur n'est pas un " + utilisateurVoulu);
			} else {
				System.out.println("Votre entrée ne correspond à aucun utilisateur");
			}
			if (!ok) {
				System.out.println("Entrez votre nouveau choix");
			}
		}
		return utilisateur;
	}

	/**
	 * permet de prendre une commande
	 * @param tableChoisie
	 * @param commande
	 * @throws Exception
	 */
	private void prendreCommande(Table tableChoisie, ArrayList<Commande> commande) throws Exception {
		if (!(UTILISATEUR_COURANT.getRole() == Utilisateur.SERVEUR)) {
			System.out.println("La table doit être occupée et vous devez être un serveur pour prendre une commande");
			optionsTable(tableChoisie);
		} else {
			System.out.println("Vous souhaitez : ");
			System.out.println("0 - Annuler la Commande");
			System.out.println("1 - Ajouter un Plat");
			System.out.println("2 - Valider la Commande");
			System.out.println("3 - Voir les 5 plats les plus populaires");
			int choix = getInt(3);
			switch (choix) {
			case 1:
				ajouterPlatCommande(tableChoisie, commande);
				break;
			case 2:
				tableChoisie.validerCommmande(commande);
				break;
			case 3:
				afficherLes5PlatsPopulaire();
				prendreCommande(tableChoisie, commande);
				break;
			default:
				optionsTable(tableChoisie);
			}
		}
	}

	/**
	 * méthode permettant de modifier la carte
	 * @throws Exception 
	 */
	private void modifierCarte() throws Exception {
		Carte carte = new Carte();
		System.out.println("0 - Retour");
		System.out.println("1 - Ajouter un plat à la carte");
		System.out.println("2 - Retirer un plat de la carte");
		System.out.println("3 - Afficher la carte");
		int choix = getInt(3);
		ArrayList<Plat> liste_plat = new ArrayList<Plat>();
		switch(choix) {
		case 0:
			ecranAccueilConnecte();
			break;
		case 1 :
			liste_plat = Plat.listPlat();
			System.out.println("Choisissez le plat à ajouter");
			int i = 0;
			for(Plat plat: liste_plat) {
				System.out.println(i+" - "+plat.getNom());
				i++;
			}
			int choix2 = getInt(liste_plat.size()-1);
			carte.ajouterPlat(liste_plat.get(choix2));
			carte.updateBDD();
			ecranAccueilConnecte();
			break;
		case 2:
			liste_plat = Plat.listPlatCarte();
			System.out.println("Choisissez le plat à retirer");
			if(liste_plat.size() == 0) {
				System.out.println("Il n'y a plus de plat à retirer");
				ecranAccueilConnecte();
			}else {
				int j = 0;
				for(Plat plat: liste_plat) {
					System.out.println(j+" - "+plat.getNom());
					j++;
				}
				int choix3 = getInt(liste_plat.size()-1);
				carte.retirerPlat(liste_plat.get(choix3));
				carte.updateBDD();
				ecranAccueilConnecte();
			}
			break;
		case 3:
			liste_plat = Plat.listPlatCarte();
			System.out.println("---------------------------------------");
			for(Plat plat: liste_plat) {
				System.out.println(plat.getNom());
			}
			System.out.println("---------------------------------------");
			ecranAccueilConnecte();
			break;
		default:
			ecranAccueilConnecte();
			break;
		}
	}

	/**
	 * permet d'ajouter un plat à une commande
	 * @param tableChoisie
	 * @param commande
	 * @throws Exception
	 */
	private void ajouterPlatCommande(Table tableChoisie, ArrayList<Commande> commande) throws Exception {
		ArrayList<String> categories = Plat.listCategories();
		if (categories.isEmpty()) {
			// pas de catégorie
			System.out.println("Aucune catégorie trouvé");
			optionsTable(tableChoisie);
		} else {
			// choix catégorie
			System.out.println("Quelle catégorie de plat ajouter ?");
			int n = 0;
			for (String categorie : categories) {
				System.out.println("		" + n++ + " - " + categorie);
			}
			String categorieChoisie = categories.get(getInt(categories.size() - 1));
			ArrayList<Plat> plats = Plat.listPlatByCategorie(categorieChoisie, true);
			System.out.println("Quel plat ajouter ?");
			n = 0;
			if (plats.isEmpty()) {
				// pas de plat
				System.out.println("Il n'y a aucun plat qui correspond");
			} else {
				// choix plat
				for (Plat plat : plats) {
					System.out.println("		" + n++ + " - " + plat.getNom());
				}
				Plat platChoisi = plats.get(getInt(plats.size() - 1));
				// choix priorité
				System.out.println("Ce plat est prioritaire ? (Menu enfant, etc.)");
				System.out.println("0 - Non");
				System.out.println("1 - Oui");
				if (getInt(1) == 1) {
					commande.add(new Commande(platChoisi, tableChoisie, Commande.ATTENTE, true, LocalDateTime.now()));
				} else {
					commande.add(new Commande(platChoisi, tableChoisie, Commande.ATTENTE, false, LocalDateTime.now()));
				}
			}
			// retour à l'écran précédent
			prendreCommande(tableChoisie, commande);
		}

	}
}
